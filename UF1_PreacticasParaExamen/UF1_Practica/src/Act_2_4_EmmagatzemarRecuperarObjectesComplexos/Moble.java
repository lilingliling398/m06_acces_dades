package Act_2_4_EmmagatzemarRecuperarObjectesComplexos;

public class Moble {
    private double ample;
    private double llarg;
    private int color;

    public Moble() {
    }

    public Moble(double ample, double llarg, int color) {
        this.ample = ample;
        this.llarg = llarg;
        this.color = color;
    }

    @Override
    public String toString() {
        return "Moble{" +
                "ample=" + ample +
                ", llarg=" + llarg +
                ", color=" + color +
                '}';
    }

    public double getAmple() {
        return ample;
    }

    public double getLlarg() {
        return llarg;
    }

    public int getColor() {
        return color;
    }


}
