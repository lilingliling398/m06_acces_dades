package Act_2_4_EmmagatzemarRecuperarObjectesComplexos;

import Util.Leer;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.Scanner;


public class Menu {
    public static void main(String[] args) throws IOException {
        String rutatxt = "D:\\Git\\Practicas\\m06_access\\habitacio.txt";
        Habitacio habitacio = fitxeroProperties(rutatxt);
        habitacio.guardarHabPerByte(rutatxt);
        boolean acabarMenu = false;
        while (!acabarMenu) {

            imprimirMenu();

            System.out.print("Opcio?: ");
            int opcion = Leer.opcionInt(0, 5);

            switch (opcion) {
                case 1:
                    System.out.println(habitacio);
                    bloquejarPantalla();
                    break;
                case 2:
                    habitacio.nouMoble();
                    bloquejarPantalla();
                    break;
                case 3:
                    habitacio.guardarHabPerByte(rutatxt);
                    bloquejarPantalla();
                    break;
                case 4:
                    Habitacio habitacio1 = llegirFitxer(rutatxt);
                    habitacio1.imprimirMuebles();
                    bloquejarPantalla();
                    break;
                case 0:
                    acabarMenu = true;
                    break;
            }
        }
    }

    private static Habitacio fitxeroProperties(String rutaHab) throws IOException {
        Habitacio habitacio;
        if (new File(rutaHab).exists()) {
            habitacio = llegirFitxer(rutaHab);
        } else {
            File filePrope = new File("D:\\Git\\Practicas\\m06_access\\hab.properties");

            Properties properties = new Properties();
            if (!filePrope.exists()) {
                properties.setProperty("ample", "100");
                properties.setProperty("llarg", "100");
                properties.setProperty("nom", "Mi habitacion");

                properties.store(new FileWriter("D:\\Git\\Practicas\\m06_access\\hab.properties"), "si");
            }

            properties.load(new FileReader("D:\\Git\\Practicas\\m06_access\\hab.properties"));

            double ample = (Double.parseDouble(properties.getProperty("ample")));
            double llarg = (Double.parseDouble(properties.getProperty("llarg")));
            String nom = (properties.getProperty("nom"));

            habitacio = new Habitacio(ample, llarg, nom);
        }
        return habitacio;
    }

    public static Habitacio llegirFitxer(String ruta) {
        Habitacio habitacio = null;
        try {
            DataInputStream llegir = new DataInputStream(new BufferedInputStream(new FileInputStream(ruta)));
            double ample = llegir.readDouble();
            double llarg = llegir.readDouble();
            String nom = llegir.readUTF();
            habitacio = new Habitacio(ample, llarg, nom);

            int cantitad = llegir.readInt();
            for (int i = 0; i < cantitad; i++) {
                double ampleM = llegir.readDouble();
                double llargM = llegir.readDouble();
                int color = llegir.readInt();

                habitacio.getMobleList().add(new Moble(ampleM, llargM, color));
            }
        } catch (IOException e) {
            System.out.println("ERROR al leer");
        }
        return habitacio;
    }


    private static void imprimirMenu() {
        System.out.println("");
        System.out.println("-MENU----------------------------------------------------");
        System.out.println("    0. Sortir");
        System.out.println("    1. Mostrar per pantalla les dades de l'habitació");
        System.out.println("    2. Afegir un moble a l'habitació");
        System.out.println("    3. Guardar a disc l'habitació");
        System.out.println("    4. Llistar per pantalla tots els mobles de l'habitació, amb totes les seves dades");
    }

    public static void bloquejarPantalla() {
        Scanner sc = new Scanner(System.in);
        System.out.print("\n'C' per a continuar ");
        boolean valido = false;
        while (!valido) {
            String valor = sc.nextLine();
            if ("C".equals(valor) || "c".equals(valor)) {
                valido = true;
            }
        }
    }
}
