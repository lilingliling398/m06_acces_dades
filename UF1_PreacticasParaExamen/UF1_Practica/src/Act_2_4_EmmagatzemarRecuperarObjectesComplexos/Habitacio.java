package Act_2_4_EmmagatzemarRecuperarObjectesComplexos;

import Util.Leer;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

public class Habitacio {
    private double ample;
    private double llarg;
    private String nom;
    List<Moble> mobleList;

    public Habitacio() {
        mobleList = new ArrayList<>();
    }

    public Habitacio(double ample, double llarg, String nom) {
        this.ample = ample;
        this.llarg = llarg;
        this.nom = nom;
        this.mobleList = new ArrayList<>();
    }

    @Override
    public String toString() {
        return "Habitacio{" +
                "\n ample=" + ample +
                "\n, llarg=" + llarg +
                "\n, nom='" + nom +
                "\n, llista muebes="+ mobleList.size();
    }

    public void imprimirMuebles() {
        System.out.println("DADES DE L'HABITACIO-------------------"+
                "\n ample=" + ample +
                "\n llarg=" + llarg +
                "\n nom='" + nom +
                "\n -DADES DEL MOBLE-");
        for (Moble moble : mobleList) {
            System.out.println(moble.toString());
        }

    }

    public void nouMoble() {
        System.out.print("Ample: ");
        double ample = Leer.valorDouble();
        System.out.println("Llarg: ");
        double llarg = Leer.valorDouble();
        System.out.println("Color: ");
        int color = Leer.valorInt();

        mobleList.add(new Moble(ample, llarg, color));
    }

    public void guardarHabPerByte(String ruta) {
        try {
            DataOutputStream writer = new DataOutputStream(new BufferedOutputStream(new FileOutputStream(ruta)));
            writer.writeDouble(ample);
            writer.writeDouble(llarg);
            writer.writeUTF(nom);

            writer.writeInt(mobleList.size());
            for (Moble moble : mobleList){
                writer.writeDouble(moble.getAmple());
                writer.writeDouble(moble.getLlarg());
                writer.writeInt(moble.getColor());
            }

            writer.close();
        }catch (IOException e){
            System.out.println("ERROR al guardar");
        }
    }



    public double getAmple() {
        return ample;
    }

    public void setAmple(double ample) {
        this.ample = ample;
    }

    public double getLlarg() {
        return llarg;
    }

    public void setLlarg(double llarg) {
        this.llarg = llarg;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public List<Moble> getMobleList() {
        return mobleList;
    }

    public void setMobleList(List<Moble> mobleList) {
        this.mobleList = mobleList;
    }
}
