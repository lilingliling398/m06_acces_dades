package Act_2_2_EmmagatzemarRecuperarMultiplesEnters;

import Util.Leer;
import java.io.*;

public class Metode {
    public static void escriureEnter(String ruta) {
        try {
            DataOutputStream streamWriter = new DataOutputStream(new BufferedOutputStream(new FileOutputStream(ruta)));
            System.out.print("Cantidad de numeros enteros que quieres escribir: ");
            int cantidad = Leer.valorInt();
            //!!hay k guarar la cantidad de numeros k voy a introducir
            streamWriter.writeInt(cantidad);
            int[] valoresWrite = new int[cantidad];

            for (int i = 0; i < cantidad; i++) {
                valoresWrite[i] = Leer.valorInt();
                streamWriter.writeInt(valoresWrite[i]);
            }
            streamWriter.close();

        } catch (IOException e) {
            System.out.println("ERROR");
        }
    }

    public static void leerEnter(String ruta) {
        try {
            DataInputStream streamReader = new DataInputStream(new BufferedInputStream(new FileInputStream(ruta)));
            int cantidad = streamReader.readInt();
            int[] array = new int[cantidad];

            for (int i = 0; i < cantidad; i++) {
                array[i] = streamReader.readInt();
                System.out.println(array[i] );
            }
            streamReader.close();
        } catch (IOException e) {
            System.out.println("ERROR");
        }
    }
}
