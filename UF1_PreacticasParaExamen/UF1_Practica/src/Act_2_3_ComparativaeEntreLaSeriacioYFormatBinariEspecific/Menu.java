package Act_2_3_ComparativaeEntreLaSeriacioYFormatBinariEspecific;

import java.io.IOException;
import java.util.Scanner;

public class Menu {
    public static void main(String[] args) throws IOException {
        String ruta = "D:\\Git\\Practicas\\m06_access\\treballadors.txt";
        Treballador treballador;
        boolean acabarMenu = false;
        while (!acabarMenu) {

            imprimirMenu();

            System.out.print("Opcio?: ");
            int opcion = Util.Leer.opcionInt(0, 4);

            switch (opcion) {
                case 1:
                    treballador = Treballador_Util.pedirTreballador();
                    treballador.emmagatzemarTreballador(ruta);
                    bloquejarPantalla();
                    break;
                case 2:
                    treballador = Treballador_Util.recuperarTreballador(ruta);
                    System.out.println(treballador.toString());
                    bloquejarPantalla();
                    break;
                case 3:
                    treballador = Treballador_Util.pedirTreballador();
                    treballador.emmagatzaemarPerByta(ruta);
                    bloquejarPantalla();
                    break;
                case 4:
                    treballador = Treballador_Util.recuperarPorByte(ruta);
                    System.out.println(treballador.toString());
                    bloquejarPantalla();
                    break;
                case 0:
                    acabarMenu = true;
                    break;
            }
        }
    }

    private static void imprimirMenu() {
        System.out.println("");
        System.out.println("-MENU----------------------------------------------------");
        System.out.println("    0. Sortir");
        System.out.println("    1. Emmagatzemar dades d'un treballador implementacion");
        System.out.println("    2. Recuperar dades d'un treballador implementacion");
        System.out.println("    3. Emmagatzemar dades d'un treballador sintactic");
        System.out.println("    4. Recuperar dades d'un treballador sintactic");

    }

    public static void bloquejarPantalla() {
        Scanner sc = new Scanner(System.in);
        System.out.print("\n'C' per a continuar ");
        boolean valido = false;
        while (!valido) {
            String valor = sc.nextLine();
            if ("C".equals(valor) || "c".equals(valor)) {
                valido = true;
            }
        }
    }
}
