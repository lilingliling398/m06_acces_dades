package Act_2_3_ComparativaeEntreLaSeriacioYFormatBinariEspecific;


import Util.Leer;
import java.io.*;
import java.util.Scanner;

public class Treballador_Util {
    static Scanner sc = new Scanner(System.in);

    public static Treballador pedirTreballador() {
        System.out.println("DADES DEL TREBALLADOR");
        System.out.print("Introduce nif: ");
        String nif = sc.nextLine();
        System.out.print("Introduce nom: ");
        String nom = sc.nextLine();
        System.out.print("Introduce sou: ");
        int sou = Leer.valorInt();

        return new Treballador(nif, nom, sou);
    }

    public static Treballador recuperarTreballador(String ruta){
        Treballador treballador = new Treballador();
        try {
            ObjectInputStream inputStream = new ObjectInputStream(new FileInputStream(ruta));
            try {
                treballador = (Treballador) inputStream.readObject();
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            }
            inputStream.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return treballador;
    }

    public static Treballador recuperarPorByte(String ruta){
        Treballador treballador = new Treballador();
        try {
            DataInputStream lectura = new DataInputStream(new BufferedInputStream(new FileInputStream(ruta)));

            treballador.setNif(lectura.readUTF());
            treballador.setNom(lectura.readUTF());
            treballador.setSou(lectura.readInt());
        }catch (IOException e) {
            System.out.println("ERROR");
        }
        return treballador;
    }

}
