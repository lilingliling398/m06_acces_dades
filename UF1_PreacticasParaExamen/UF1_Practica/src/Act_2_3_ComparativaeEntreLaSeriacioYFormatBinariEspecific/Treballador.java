package Act_2_3_ComparativaeEntreLaSeriacioYFormatBinariEspecific;

import java.io.*;


public class Treballador implements Serializable, Cloneable {
    private String nif;
    private String nom;
    private int sou;

    public Treballador(String nif, String nom, int sou) {
        this.nif = nif;
        this.nom = nom;
        this.sou = sou;
    }

    public Treballador() {
    }

    @Override
    public String toString() {
        return "Treballador{" +
                " nif='" + nif + '\'' +
                ", nom='" + nom + '\'' +
                ", sou=" + sou +
                '}';
    }

    public void emmagatzemarTreballador(String ruta) {
        try {
            ObjectOutputStream sortida = new ObjectOutputStream(new FileOutputStream(ruta));
            sortida.writeObject(this);
            sortida.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void emmagatzaemarPerByta(String ruta) {
        try {
            DataOutputStream escriptura = new DataOutputStream(new BufferedOutputStream(new FileOutputStream(ruta)));
            escriptura.writeUTF(this.nif);
            escriptura.writeUTF(this.nom);
            escriptura.writeInt(this.sou);
            escriptura.close();
        }catch (IOException e) {
            System.out.println("ERROR");
        }
    }

    public void setNif(String nif) {
        this.nif = nif;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public void setSou(int sou) {
        this.sou = sou;
    }
}
