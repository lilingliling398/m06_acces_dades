package Act_1_3_ComparadorDeFitxers;

import Util.FileUtil;

import java.io.File;
import java.io.IOException;
import java.util.logging.SocketHandler;

public class Main {
    public static void main(String[] args) throws IOException {
        File dirA = new File("D:\\Git\\Practicas\\m06_access\\DirA");
        File dirB = new File("D:\\Git\\Practicas\\m06_access\\DirB");
        FileUtil.crearDirectoriYFitxer(dirA);
        FileUtil.crearDirectoriYFitxer(dirB);

        System.out.println("- S’està comparant els següents dos directoris");
        System.out.println("---- Directori A: "+dirA);
        System.out.println("---- Directori B: "+dirB);

        compararDir(dirA, dirB);
    }

    public static void compararDir(File dirA, File dirB) {
        File[] listA = dirA.listFiles();
        File[] listB = dirB.listFiles();
        if (listA == null || listB == null) {
            System.out.println("Vacio");
        }else {
            for (File fitxerA : listA) {
                boolean iguals = false;
                for (File fitxerB : listB) {
                    if (fitxerA.getName().equals(fitxerB.getName())) {
                        System.out.println("\n- El fitxer “" + fitxerA.getName() + "” existeix a ambdós directoris");
                        iguals = true;
                        comparaData(fitxerA, fitxerB);
                        compararTamany(fitxerA, fitxerB);
                    }
                }
                if (!iguals) {
                    System.out.println("\n- El fitxer “" + fitxerA.getName() + "” existeix al directori A, però no existeix al directori B");
                }
            }
        }
    }

    public static void compararTamany(File fitxerA, File fitxerB) {
        if (fitxerA.length() == fitxerB.length()) {
            System.out.println("---- Tenen la mateixa mida");
        }else if (fitxerA.length() > fitxerB.length()) {
            System.out.println("---- El fitxer al directori A és més gran que a B");
        }else {
            System.out.println("---- El fitxer al directori B és més gran que a A");
        }
    }

    public static void comparaData(File dirA, File dirB) {
        if (dirA.lastModified() == dirB.lastModified()) {
            System.out.println("---- Tenen la mateixa data");
        }else if (dirA.lastModified() > dirB.lastModified()) {
            System.out.println("---- El fitxer al directori A és més nou (segons la data de modificació) que a B");
        }else {
            System.out.println("---- El fitxer al directori B és més nou que a A");
        }
    }
}
