package Act_3_2_EmmagatzematXML;

import Util.Leer;
import org.xml.sax.SAXException;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.xpath.XPathExpressionException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Menu {
    public static void main(String[] args) throws IOException, SAXException, ParserConfigurationException, TransformerException, XPathExpressionException {
        String ruta = "D:\\Git\\Practicas\\m06_access\\UF1_Practica\\fitxer.xml";
        List<Assignatura> assignaturaList = new ArrayList<>();

        boolean acabarMenu = false;
        while (!acabarMenu) {

            imprimirMenu();

            System.out.print("Opcio?: ");
            int opcion = Leer.opcionInt(0, 6);

            switch (opcion) {
                case 1:
                    assignaturaList = Metode.llegirSequencial(ruta);
                    bloquejarPantalla();
                    break;
                case 2:
                    assignaturaList = Metode.llegirXMLSintactic(ruta);
                    bloquejarPantalla();
                    break;
                case 3:
                    for (Assignatura assignatura : assignaturaList) {
                        assignatura.imiprimir();
                    }
                    bloquejarPantalla();
                    break;
                case 4:

                    bloquejarPantalla();
                    break;
                case 5:

                    bloquejarPantalla();
                    break;
                case 6:
                    Metode.guardarXML(assignaturaList);
                    bloquejarPantalla();
                    break;
                case 0:
                    acabarMenu = true;
                    break;
            }
        }
    }

    private static void imprimirMenu() {
        System.out.println("");
        System.out.println("-MENU----------------------------------------------------");
        System.out.println("    0. Sortir");
        System.out.println("    1. Llegir d'un fitxer XML pel mètode seqüencial");
        System.out.println("    2. Llegir d'un fitxer XML pel mètode sintàctic");
        System.out.println("    3. Mostrar per pantalla totes les assignatures amb les seves dades (número, nom, durada i la llista d'alumnes)");
        System.out.println("    4. Afegir una assignatura, es demanaran les dades bàsiques (número, nom i durada)");
        System.out.println("    5. Afegir un alumne a una assignatura");
        System.out.println("    6. Guardar a disc en XML amb les assignatures");
    }

    public static void bloquejarPantalla() {
        Scanner sc = new Scanner(System.in);
        System.out.print("\n'C' per a continuar ");
        boolean valido = false;
        while (!valido) {
            String valor = sc.nextLine();
            if ("C".equals(valor) || "c".equals(valor)) {
                valido = true;
            }
        }
    }
}
