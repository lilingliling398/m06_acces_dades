package Act_3_2_EmmagatzematXML;

public class Alumne {
    private String nom;
    private String dni;
    private boolean repetidor;

    public Alumne() {

    }

    public void imprimir() {
        System.out.println("        =============");
        System.out.println("        Nom: "+nom);
        System.out.println("        DNI: "+dni);
        System.out.println("        Es repetidor: "+repetidor);
    }

    public Alumne(String nom, String dni, boolean repetidor) {
        this.nom = nom;
        this.dni = dni;
        this.repetidor = repetidor;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getDni() {
        return dni;
    }

    public void setDni(String dni) {
        this.dni = dni;
    }

    public boolean isRepetidor() {
        return repetidor;
    }

    public void setRepetidor(boolean repetidor) {
        this.repetidor = repetidor;
    }
}
