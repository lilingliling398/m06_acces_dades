package Act_3_2_EmmagatzematXML;

import java.util.ArrayList;
import java.util.List;

public class Assignatura {
    private int numero;
    private String nom;
    private int durada;
    private List<Alumne> alumneList;

    public Assignatura() {
        alumneList = new ArrayList<>();
    }

    public Assignatura(int numero, String nom, int durada) {
        this.numero = numero;
        this.nom = nom;
        this.durada = durada;
        this.alumneList = new ArrayList<>();
    }

    public void imiprimir() {
        System.out.println("===========================");
        System.out.println("Numero: "+numero);
        System.out.println("Nom: "+nom);
        System.out.println("Durada: "+durada);
        for (Alumne alumne : alumneList) {
            alumne.imprimir();
        }
    }

    public int getNumero() {
        return numero;
    }

    public void setNumero(int numero) {
        this.numero = numero;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public int getDurada() {
        return durada;
    }

    public void setDurada(int durada) {
        this.durada = durada;
    }

    public List<Alumne> getAlumneList() {
        return alumneList;
    }

    public void setAlumneList(List<Alumne> alumneList) {
        this.alumneList = alumneList;
    }
}
