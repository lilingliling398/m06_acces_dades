package Act_3_2_EmmagatzematXML;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;


import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;
import java.io.File;
import java.io.IOException;
import java.lang.invoke.VarHandle;
import java.util.ArrayList;
import java.util.List;

public class Metode {

    public static List<Assignatura> llegirSequencial(String ruta) throws ParserConfigurationException, IOException, SAXException {
        List<Assignatura> assignaturaList = new ArrayList<>();
        Document doc = DocumentBuilderFactory.newInstance().newDocumentBuilder().parse(ruta);
        Node arrel = doc.getDocumentElement();
        NodeList nodeListAssignatures = arrel.getChildNodes();
        for (int i = 0; i < nodeListAssignatures.getLength(); i++) {
            //cada assignatura
            if (nodeListAssignatures.item(i).getNodeName().equals("assignatura")) {
                Assignatura assignaturaNou = new Assignatura();
                NodeList nodeListAssig = nodeListAssignatures.item(i).getChildNodes();
                for (int j = 0; j < nodeListAssig.getLength(); j++) {
                    String textContent1 = nodeListAssig.item(j).getTextContent();
                    if (nodeListAssig.item(j).getNodeName().equals("numero"))
                        assignaturaNou.setNumero(Integer.parseInt(textContent1));

                    if (nodeListAssig.item(j).getNodeName().equals("nom"))
                        assignaturaNou.setNom(textContent1);

                    if (nodeListAssig.item(j).getNodeName().equals("durada"))
                        assignaturaNou.setDurada(Integer.parseInt(textContent1));
                    //lista alumnes
                    if (nodeListAssig.item(j).getNodeName().equals("alumnes")){
                        NodeList nodeListAlumnes = nodeListAssig.item(j).getChildNodes();
                        for (int k = 0; k < nodeListAlumnes.getLength(); k++) {
                             //cada alumne
                            if (nodeListAlumnes.item(k).getNodeName().equals("alumne")) {

                                Alumne alumneNou = new Alumne();
                                NodeList nodeListAlumne = nodeListAlumnes.item(k).getChildNodes();
                                for (int l = 0; l < nodeListAlumne.getLength(); l++) {
                                    String textContent = nodeListAlumne.item(l).getTextContent();
                                    if (nodeListAlumne.item(l).getNodeName().equals("nom")){
                                        alumneNou.setNom(textContent);
                                    }

                                    if (nodeListAlumne.item(l).getNodeName().equals("dni")){
                                        alumneNou.setDni(textContent);
                                    }

                                    if (nodeListAlumne.item(l).getNodeName().equals("repetidor")) {
                                        alumneNou.setRepetidor(Boolean.parseBoolean(textContent));
                                    }
                                }
                                assignaturaNou.getAlumneList().add(alumneNou);
                            }


                        }
                    }

                }
                assignaturaList.add(assignaturaNou);
            }
        }

        return assignaturaList;
    }

    public static List<Assignatura> llegirXMLSintactic(String ruta) throws ParserConfigurationException, IOException, SAXException, XPathExpressionException {
        Document doc = DocumentBuilderFactory.newInstance().newDocumentBuilder().parse(ruta);
        List<Assignatura> assignaturaList = new ArrayList<>();


        XPathExpression exprAssignatura = XPathFactory.newInstance().newXPath().compile("/assignaturas/assignatura");
        NodeList llistaNodeAssignatura = (NodeList) exprAssignatura.evaluate(doc, XPathConstants.NODESET);

        for (int i = 0; i < llistaNodeAssignatura.getLength(); i++) {
            Assignatura assignatura = new Assignatura();
            XPathExpression exprNumero = XPathFactory.newInstance().newXPath().compile("numero");
            NodeList numero = (NodeList) exprNumero.evaluate(llistaNodeAssignatura.item(i), XPathConstants.NODESET);
            assignatura.setNumero(Integer.parseInt(numero.item(0).getTextContent()));

            XPathExpression exprNom = XPathFactory.newInstance().newXPath().compile("nom");
            NodeList nom = (NodeList) exprNom.evaluate(llistaNodeAssignatura.item(i), XPathConstants.NODESET);
            assignatura.setNom(nom.item(0).getTextContent());

            XPathExpression exprDurada = XPathFactory.newInstance().newXPath().compile("durada");
            NodeList durada = (NodeList) exprDurada.evaluate(llistaNodeAssignatura.item(i),XPathConstants.NODESET);
            assignatura.setDurada(Integer.parseInt(durada.item(0).getTextContent()));


            XPathExpression exprAlumne = XPathFactory.newInstance().newXPath().compile("alumnes/alumne");
            NodeList alumne = (NodeList) exprAlumne.evaluate(llistaNodeAssignatura.item(i),XPathConstants.NODESET);
            for (int j = 0; j < alumne.getLength(); j++) {
                Alumne alumneNou = new Alumne();

                XPathExpression exprNomA = XPathFactory.newInstance().newXPath().compile("nom");
                NodeList nomA = (NodeList) exprNomA.evaluate(alumne.item(j), XPathConstants.NODESET);
                alumneNou.setNom(nomA.item(0).getTextContent());

                XPathExpression exprDni = XPathFactory.newInstance().newXPath().compile("dni");
                NodeList dni = ( NodeList) exprDni.evaluate(alumne.item(j), XPathConstants.NODESET);
                alumneNou.setDni(dni.item(0).getTextContent());

                XPathExpression exprRepetidor = XPathFactory.newInstance().newXPath().compile("repetidor");
                NodeList repetidor = (NodeList) exprRepetidor.evaluate(alumne.item(j), XPathConstants.NODESET);
                alumneNou.setRepetidor(Boolean.parseBoolean(repetidor.item(0).getTextContent()));

                assignatura.getAlumneList().add(alumneNou);
            }
            assignaturaList.add(assignatura);
        }
        return assignaturaList;
    }

    public static void guardarXML(List<Assignatura> assignaturaList) throws ParserConfigurationException, TransformerException {


        Document document = DocumentBuilderFactory.newInstance().newDocumentBuilder().newDocument();

        Element assignatures = document.createElement("assignatures");
        document.appendChild(assignatures);

        for (Assignatura assignatura : assignaturaList) {
            Element element_assignatura = document.createElement("assignatura");
            assignatures.appendChild(element_assignatura);

            Element numero = document.createElement("numero");
            element_assignatura.appendChild(numero);
            numero.setTextContent(String.valueOf(assignatura.getNumero()));

            Element nom = document.createElement("nom");
            element_assignatura.appendChild(nom);
            nom.setTextContent(assignatura.getNom());

            Element durada = document.createElement("durada");
            element_assignatura.appendChild(durada);
            durada.setTextContent(String.valueOf(assignatura.getDurada()));

            Element alumnes = document.createElement("alumnes");
            element_assignatura.appendChild(alumnes);

            for (Alumne alumne1 : assignatura.getAlumneList()) {

                Element alumne = document.createElement("alumne");
                alumnes.appendChild(alumne);

                Element nomAlum = document.createElement("nom");
                alumne.appendChild(nomAlum);
                nomAlum.setTextContent(alumne1.getNom());

                Element dni = document.createElement("dni");
                alumne.appendChild(dni);
                dni.setTextContent(alumne1.getDni());

                Element repetidor = document.createElement("repetidor");
                alumne.appendChild(repetidor);
                repetidor.setTextContent(String.valueOf(alumne1.isRepetidor()));
            }

        }
        Transformer trans = TransformerFactory.newInstance().newTransformer();
        StreamResult result = new StreamResult(new File("fitxerdisc.xml"));
        DOMSource source = new DOMSource(document);
        trans.transform(source, result);
    }



}
