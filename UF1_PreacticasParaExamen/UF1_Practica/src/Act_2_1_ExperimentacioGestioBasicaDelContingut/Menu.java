package Act_2_1_ExperimentacioGestioBasicaDelContingut;

import java.io.IOException;
import java.util.Scanner;

public class Menu {
    public static void main(String[] args) throws IOException {
        String ruta = "D:\\Git\\Practicas\\m06_access\\texto.txt";

        boolean acabarMenu = false;
        while (!acabarMenu) {

            imprimirMenu();

            System.out.print("Opcio?: ");
            int opcion = Util.Leer.opcionInt(0, 5);

            switch (opcion) {
                case 1:
                    Metode.guardarPorByte(ruta);
                    bloquejarPantalla();
                    break;
                case 2:
                    Metode.guardarPorCaracter(ruta);
                    bloquejarPantalla();
                    break;
                case 3:

                    bloquejarPantalla();
                    break;
                case 0:
                    acabarMenu = true;
                    break;
            }
        }
    }

    private static void imprimirMenu() {
        System.out.println("");
        System.out.println("-MENU----------------------------------------------------");
        System.out.println("    0. Sortir");
        System.out.println("    1. Emmagatzemi en un fitxer, per bytes");
        System.out.println("    2. Emmagatzemi en un fitxer, per caràcters i en codificació UTF-8");
        System.out.println("    3. ");
    }

    public static void bloquejarPantalla() {
        Scanner sc = new Scanner(System.in);
        System.out.print("\n'C' per a continuar ");
        boolean valido = false;
        while (!valido) {
            String valor = sc.nextLine();
            if ("C".equals(valor) || "c".equals(valor)) {
                valido = true;
            }
        }
    }
}
