package Act_2_1_ExperimentacioGestioBasicaDelContingut;

import javax.naming.InsufficientResourcesException;
import javax.print.attribute.standard.MediaSize;
import java.io.*;
import java.nio.charset.StandardCharsets;
import java.util.Properties;

public class Metode {
    public static void guardarPorByte(String ruta) {
        try {
            DataOutputStream streamEscriptura = new DataOutputStream(new BufferedOutputStream(new FileOutputStream(ruta, true)));
            streamEscriptura.writeBoolean(true);
            streamEscriptura.writeInt(123300);
            streamEscriptura.writeUTF("hola ");

            streamEscriptura.close();

            DataInputStream streamLectura = new DataInputStream(new BufferedInputStream(new FileInputStream(ruta)));
            System.out.println(streamLectura.readBoolean() + " " + streamLectura.readInt() + " " + streamLectura.readUTF() + "" + streamLectura.readBoolean() + " " + streamLectura.readInt() + " " + streamLectura.readUTF());
            streamLectura.close();
        } catch (IOException e) {
            System.out.println("ERROR");
        }
    }

    public static void guardarPorCaracter(String ruta) {
        Properties properties = new Properties();
        try {
            OutputStreamWriter streamWriter = new OutputStreamWriter(new FileOutputStream(ruta, true), StandardCharsets.ISO_8859_1);
            streamWriter.append("123");

            streamWriter.close();

            InputStreamReader streamReader = new InputStreamReader(new FileInputStream(ruta), StandardCharsets.ISO_8859_1);
            /*int charLlegits = 0;
            while ((charLlegits = streamReader.read()) != -1) {
                System.out.println((char) charLlegits);
            }*/
            char[] cadena = new char[1000];
            if (streamReader.read(cadena) != -1)
                System.out.println(cadena);
            streamReader.close();
        } catch (IOException e) {
            System.out.println("ERROR");
        }
    }
}
