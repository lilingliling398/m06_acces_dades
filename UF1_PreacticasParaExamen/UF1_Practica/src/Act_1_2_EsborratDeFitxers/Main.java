package Act_1_2_EsborratDeFitxers;

import Util.FileUtil;
import java.io.File;
import java.io.IOException;


public class Main {
    public static void main(String[] args) throws IOException {
        String rutaDir = "D:\\DIRTEST";

        File directorio = new File(rutaDir);

        FileUtil.crearDirectoriYFitxer(directorio);
        esborrarDirectori(directorio);
    }

    public static void esborrarDirectori(File directorio) {
        if (directorio.exists()) {
            File[] listFile = directorio.listFiles();
            if (listFile == null) {
                System.out.println("Res que esborrar");
            } else {
                System.out.println("\n__Esborrant___");
                for (File fitxer : listFile) {
                    intentarBorrar(fitxer);
                }
            }
            intentarBorrar(directorio);

        } else {
            System.out.println("ERROR, no existeix la ruta del directo " + directorio.getPath());
        }
    }

    public static void intentarBorrar(File file) {
        //siempre lo ultimo borrar
        if (file.isDirectory()) {
            if (file.delete()) {
                System.out.println("S'ha esborrat el directori " + file.getName());
            } else {
                System.out.println("No s'ha esborrat correctament el directori " + file.getName());
            }
        } else {
            if (file.delete()) {
                System.out.println("S'ha esborrat el fitxer " + file.getName());
            } else {
                System.out.println("No s'ha esborrat correctament el fitxer " + file.getName());
            }
        }
    }


}
