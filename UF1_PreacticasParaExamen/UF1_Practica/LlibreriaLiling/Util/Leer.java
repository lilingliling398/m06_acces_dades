package Util;

import java.util.Scanner;

public class Leer {
    static Scanner sc = new Scanner(System.in);

    public static int valorInt() {
        String valor = sc.nextLine();
        int resultat = 0;
        try {
            resultat = Integer.parseInt(valor);
        } catch (NumberFormatException e) {
            System.out.print("Introdueix un int: ");
            resultat = valorInt();
        }
        return resultat;
    }

    public static int opcionInt(int min, int max) {
        int opcion = valorInt();
        if (opcion >= min && opcion <= max) {
            return opcion;
        } else {
            System.out.print("Introdueix un int entre " + min + "-" + max + ": ");
            opcion = opcionInt(min, max);
        }
        return opcion;
    }

    public static double valorDouble() {
        String valor = sc.nextLine();
        double resultat = 0;
        try {
            resultat = Double.parseDouble(valor);
        }catch (NumberFormatException e) {
            System.out.println("Introdueix un double: ");
            resultat = valorDouble();
        }
        return resultat;
    }
}


