package Util;

import java.io.File;
import java.io.IOException;


public class FileUtil {
    public static void crearDirectoriYFitxer(File directorio) throws IOException {
        if (directorio.exists()) {
            System.out.println("El directori ya existeix " + directorio);
        } else {
            if (directorio.mkdir()) {
                System.out.println("Ruta del directori: " + directorio);
                System.out.print("Numero de fitxer que vols crear: ");
                int num = Leer.valorInt();

                for (int i = 1; i <= num; i++) {
                    File fitxer = new File(directorio + "\\fitxer" + i + ".txt");
                    if (fitxer.createNewFile()) {
                        System.out.println("S'ha creat el fitxer " + fitxer.getName());
                    } else {
                        System.out.println("");
                    }

                }
            } else {
                System.out.println("ERROR, no s'ha creat correctament el directori " + directorio.getName());
            }
        }
    }

}
