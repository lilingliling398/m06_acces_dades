package liling_act_2_2_ExperimentacioJavaMongoDB;


import java.util.ArrayList;
import java.util.List;

public class Assignatura {
    String nom;
    int hores_setmana;
    List<Alumne> alumne_matriculats;

    public Assignatura(String nom, int hores_setmana) {
        this.nom = nom;
        this.hores_setmana = hores_setmana;
        alumne_matriculats= new ArrayList<>();
    }

    public Assignatura(String nom, int hores_setmana, List<Alumne> alumne_matriculats) {
        this.nom = nom;
        this.hores_setmana = hores_setmana;
        this.alumne_matriculats = alumne_matriculats;
    }
}
