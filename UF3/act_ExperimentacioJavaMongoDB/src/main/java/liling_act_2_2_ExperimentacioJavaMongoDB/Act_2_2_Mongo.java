package liling_act_2_2_ExperimentacioJavaMongoDB;

import com.mongodb.client.*;
import com.mongodb.client.result.DeleteResult;
import org.bson.Document;

import javax.print.Doc;
import java.util.ArrayList;
import java.util.List;

import static com.mongodb.client.model.Filters.*;

public class Act_2_2_Mongo {
    protected static void inserir(Assignatura assignatura) {
        try (MongoClient mongoClient = MongoClients.create()) {
            MongoDatabase database = mongoClient.getDatabase("bd_prova");
            MongoCollection<Document> collection = database.getCollection("assignatures");


            List<Document> arrayAlumnes = new ArrayList<>();
            for (Alumne alumne : assignatura.alumne_matriculats){
                arrayAlumnes.add(new Document("dni", alumne.dni).append("nom", alumne.nom));
            }

            Document document = new Document("nom", assignatura.nom).append("hores_setmana", assignatura.hores_setmana).append("alumnes_matriculats", arrayAlumnes);
            collection.insertOne(document);
        }
    }

    public static void esborrarTot() {
        try (MongoClient mongoClient = MongoClients.create()) {
            MongoDatabase database = mongoClient.getDatabase("bd_prova");
            MongoCollection<Document> collection = database.getCollection("assignatures");

            DeleteResult deleteResult = collection.deleteMany(gt("hores_setmana", 0));
            System.out.println("S'ha esborrat " + deleteResult.getDeletedCount() + " assignatures");
        }
    }

    public static List<Assignatura> llegirAssignatures(int alumnesMaxim) {
        List<Assignatura> assignaturas = new ArrayList<>();
        try (MongoClient mongoClient = MongoClients.create()) {
            MongoDatabase database = mongoClient.getDatabase("bd_prova");
            MongoCollection<Document> collection = database.getCollection("assignatures");

            try (MongoCursor<Document> cursor = collection.find(where("this.alumnes_matriculats.length<="+alumnesMaxim)).iterator()) {
                while (cursor.hasNext()) {
                    Document d = cursor.next();
                    String nom = d.getString("nom");
                    int hores = d.getInteger("hores_setmana");
                    List<Document> alumnes_matriculades = d.getList("alumnes_matriculats", Document.class);
                    List<Alumne> alumneList = new ArrayList<>();
                    for (Document subDocument : alumnes_matriculades){
                        Alumne alumne = new Alumne(subDocument.getInteger("dni"), subDocument.getString("nom"));
                        alumneList.add(alumne);
                    }
                    assignaturas.add(new Assignatura(nom, hores,alumneList));
                }
            }
        }
        return assignaturas;
    }


}
