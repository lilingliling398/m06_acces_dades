package liling_act_2_2_ExperimentacioJavaMongoDB;

import java.util.ArrayList;
import java.util.List;

public class Main {
    public static void main(String[] args) {
        List<Alumne> alumneList = new ArrayList<>();
        alumneList.add(new Alumne(1, "Liling1"));
        alumneList.add(new Alumne(2, "Liling2"));
        alumneList.add(new Alumne(3, "Liling3"));

        List<Alumne> alumneList2 = new ArrayList<>();
        alumneList2.add(new Alumne(1, "Liling1"));
        alumneList2.add(new Alumne(2, "Liling2"));
        alumneList2.add(new Alumne(3, "Liling3"));
        alumneList2.add(new Alumne(4, "Liling4"));
        alumneList2.add(new Alumne(5, "Liling5"));
        alumneList2.add(new Alumne(6, "Liling6"));


        Assignatura assignatura1 = new Assignatura("acces dades", 5, alumneList);
        Assignatura assignatura2 = new Assignatura("programacion", 3, alumneList2);
        Assignatura assignatura3 = new Assignatura("empresa", 1, alumneList);

        Act_2_2_Mongo.esborrarTot();
        Act_2_2_Mongo.inserir(assignatura1);
        Act_2_2_Mongo.inserir(assignatura2);
        Act_2_2_Mongo.inserir(assignatura3);



        int almunesMax = 6;
        List<Assignatura> assignaturas = Act_2_2_Mongo.llegirAssignatures(almunesMax);


        System.out.println("Assignatures amb "+almunesMax+" alumnes com a maxim:");
        for (Assignatura assignatura : assignaturas) {
            System.out.println("ASSIGNATURA: " + assignatura.nom + " - HORES: " + assignatura.hores_setmana);
            System.out.println("ALUMNES MATRICULADES:");
            for (Alumne alumne :assignatura.alumne_matriculats){
                System.out.println("    nom: "+ alumne.nom+" - dni: "+alumne.dni);
            }
        }
    }
}
