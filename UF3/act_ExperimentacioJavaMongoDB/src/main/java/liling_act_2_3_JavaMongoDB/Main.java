package liling_act_2_3_JavaMongoDB;

import java.util.List;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {


        boolean acabarMenu = false;
        while (!acabarMenu) {

            imprimirMenu();

            System.out.print("Opcio?: ");
            int opcion = Util.Leer.pedirInt(0, 6);
            System.out.println();

            switch (opcion) {
                case 1:
                    Act_2_3.afegirClient();
                    bloquejarPantalla();
                    break;
                case 2:
                    Act_2_3.llistarNif();
                    bloquejarPantalla();
                    break;
                case 3:
                    Act_2_3.esborrarTot();
                    bloquejarPantalla();
                    break;
                case 4:
                    Act_2_3.nouComanda();
                    bloquejarPantalla();
                    break;
                case 5:
                    List<Client> clients = Act_2_3.buscarClientFacturacio();
                    for (Client client : clients) {
                        System.out.println(client);
                    }
                    bloquejarPantalla();
                    break;
                case 6:
                    List<Client> clients1 = Act_2_3.buscarClientComanda();
                    for (Client client : clients1) {
                        System.out.println(client);
                    }
                    bloquejarPantalla();
                    break;

                case 0:
                    acabarMenu = true;
                    break;
            }
        }
    }

    private static void imprimirMenu() {
        System.out.println("");
        System.out.println("-MENU----------------------------------------------------");
        System.out.println("    1. Afegir client");
        System.out.println("    2. Llistar NIF");
        System.out.println("    3. Esborrar tot");
        System.out.println("    4. Afegir comanda nou");
        System.out.println("    5. Llistar clients amb minim de facturacio");
        System.out.println("    6. Llistar clients amb minim de comandes");
        System.out.println("    0. Sortir");
    }

    public static void bloquejarPantalla() {
        Scanner sc = new Scanner(System.in);
        System.out.print("\n'C' per a continuar ");
        boolean valido = false;
        while (!valido) {
            String valor = sc.nextLine();
            if ("C".equals(valor) || "c".equals(valor)) {
                valido = true;
            }
        }
    }

}
