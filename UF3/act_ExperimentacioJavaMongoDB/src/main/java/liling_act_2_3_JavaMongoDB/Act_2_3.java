package liling_act_2_3_JavaMongoDB;

import Util.Leer;
import com.mongodb.client.*;
import com.mongodb.client.result.DeleteResult;
import org.bson.Document;

import java.time.LocalDate;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Scanner;

import static com.mongodb.client.model.Filters.*;

public class Act_2_3 {
    //exercici1
    protected static void afegirClient(Client client) {
        try (MongoClient mongoClient = MongoClients.create()) {
            MongoDatabase database = mongoClient.getDatabase("bd_prova");
            MongoCollection<Document> collection = database.getCollection("clients");

            //guardar
            List<Document> arrayList = new ArrayList<>();
            for (Comanda comanda : client.comandes) {
                arrayList.add(new Document("data_comanda", comanda.data_comanda)
                        .append("imports", comanda.imports)
                        .append("pagada", comanda.pagada)
                );
            }

            Document document = new Document();
            document.append("nif", client.nif);
            document.append("nom", client.nom);
            document.append("total_facturacio", client.total_facturacio);
            if (client.telefon != null) {
                document.append("telefon", client.telefon);
            }
            if (client.correu != null) {
                document.append("correu", client.correu);
            }
            if (client.comandes.size() != 0) {
                document.append("comandes", arrayList);
            }

            collection.insertOne(document);

        }
    }


    //exercici2
    protected static void afegirClient() {
        Scanner sc = new Scanner(System.in);
        try (MongoClient mongoClient = MongoClients.create()) {
            MongoDatabase database = mongoClient.getDatabase("bd_prova");
            MongoCollection<Document> collection = database.getCollection("clients");

            //pedir cliente
            System.out.print("NIF: ");
            String nif = sc.nextLine();

            System.out.print("Nom: ");
            String nom = sc.nextLine();

            System.out.print("Total facturació: ");
            int facturacio = Leer.pedirInt();

            System.out.print("Telefon: ");
            String telefon = stringIsNull();

            System.out.print("Correu: ");
            String correu = stringIsNull();

            Client client = new Client(nif, nom, facturacio);
            client.telefon = telefon;
            client.correu = correu;

            //guardar
            List<Document> arrayList = new ArrayList<>();
            for (Comanda comanda : client.comandes) {
                arrayList.add(new Document("data_comanda", comanda.data_comanda)
                        .append("imports", comanda.imports)
                        .append("pagada", comanda.pagada)
                );
            }

            Document document = new Document();
            document.append("nif", client.nif);
            document.append("nom", client.nom);
            document.append("total_facturacio", client.total_facturacio);
            if (client.telefon != null) {
                document.append("telefon", client.telefon);
            }
            if (client.correu != null) {
                document.append("correu", client.correu);
            }
            if (client.comandes.size() != 0) {
                document.append("comandes", arrayList);
            }

            collection.insertOne(document);

        }
    }

    //exercici3
    public static void llistarNif() {
        try (MongoClient mongoClient = MongoClients.create()) {
            MongoDatabase database = mongoClient.getDatabase("bd_prova");
            MongoCollection<Document> collection = database.getCollection("clients");

            imprimirNIF(collection);

        }
    }

    //exercici4
    public static void nouComanda() {
        try (MongoClient mongoClient = MongoClients.create()) {
            MongoDatabase database = mongoClient.getDatabase("bd_prova");
            MongoCollection<Document> collection = database.getCollection("clients");

            List<String> listNIF = imprimirNIF(collection);

            int index = Leer.pedirInt(0, listNIF.size());
            Client clientEscollit = buscarClient(collection, listNIF.get(index));
            System.out.println(clientEscollit);

            System.out.println("==NOU DADES==");
            System.out.print("Data comanda: ");
            Date data = Leer.pedirData();
            System.out.print("Import: ");
            int imports = Leer.pedirInt();


            Comanda comandaNou = new Comanda(data, imports);


            clientEscollit.total_facturacio = clientEscollit.total_facturacio + comandaNou.imports;

            clientEscollit.comandes.add(comandaNou);

            esborrarClient(collection, listNIF.get(index));
            afegirClient(clientEscollit);
            System.out.println("Client " + clientEscollit.nom + " updated");

        }

    }

    //exercici5
    public static List<Client> buscarClientFacturacio() {
        System.out.print("Facturacio minim: ");
        int factura = Leer.pedirInt();
        try (MongoClient mongoClient = MongoClients.create()) {
            MongoDatabase database = mongoClient.getDatabase("bd_prova");
            MongoCollection<Document> collection = database.getCollection("clients");

            List<Client> clients = new ArrayList<>();
            try (MongoCursor<Document> cursor = collection.find(gt("total_facturacio", factura)).iterator()) {
                recuperarClients(cursor, clients);
            }
            return clients;
        }
    }


    //exercici6
    public static List<Client> buscarClientComanda() {
        System.out.print("Comandes minim: ");
        int numComandes = Leer.pedirInt();
        try (MongoClient mongoClient = MongoClients.create()) {
            MongoDatabase database = mongoClient.getDatabase("bd_prova");
            MongoCollection<Document> collection = database.getCollection("clients");

            List<Client> clients = new ArrayList<>();
            try (MongoCursor<Document> cursor = collection.find(where("this.comandes && this.comandes.length>=" + numComandes)).iterator()) {
                recuperarClients(cursor, clients);
            }
            return clients;
        }
    }


    public static void recuperarClients(MongoCursor<Document> cursor, List<Client> clients) {
        while (cursor.hasNext()) {
            Document d = cursor.next();
            String nifd = d.getString("nif");
            String nom = d.getString("nom");
            int total_facturacio = d.getInteger("total_facturacio");

            Client clientTrobat = new Client(nifd, nom, total_facturacio);
            if (d.containsKey("telefon")) {
                clientTrobat.telefon = d.getString("telefon");
            }

            if (d.containsKey("correu")) {
                clientTrobat.correu = d.getString("correu");
            }

            List<Comanda> comandaList = new ArrayList<>();
            if (d.containsKey("comandes")) {
                List<Document> docComandes = d.getList("comandes", Document.class);
                for (Document subDoc : docComandes) {
                    comandaList.add(new Comanda(subDoc.getDate("data_comanda"), subDoc.getInteger("imports")));
                }
                clientTrobat.comandes = comandaList;

            }
            clients.add(clientTrobat);
        }
    }

    public static Client buscarClient(MongoCollection<Document> collection, String nif) {
        Client client = null;
        try (MongoCursor<Document> cursor = collection.find(eq("nif", nif)).iterator()) {
            while (cursor.hasNext()) {
                Document d = cursor.next();
                String nifd = d.getString("nif");
                String nom = d.getString("nom");
                int total_facturacio = d.getInteger("total_facturacio");

                client = new Client(nifd, nom, total_facturacio);
                if (d.containsKey("telefon")) {
                    client.telefon = d.getString("telefon");
                }

                if (d.containsKey("correu")) {
                    client.correu = d.getString("correu");
                }

                List<Comanda> comandaList = new ArrayList<>();
                if (d.containsKey("comandes")) {
                    List<Document> docComandes = d.getList("comandes", Document.class);
                    for (Document subDoc : docComandes) {
                        comandaList.add(new Comanda(subDoc.getDate("data_comanda"), subDoc.getInteger("imports")));
                    }
                    client.comandes = comandaList;
                }
            }
        }
        return client;
    }

    public static void esborrarClient(MongoCollection<Document> collection, String nif) {
        DeleteResult deleteResult = collection.deleteMany(eq("nif", nif));
        System.out.println("S'ha esborrat " + deleteResult.getDeletedCount() + " client amb nif: " + nif);
    }

    public static List<String> imprimirNIF(MongoCollection<Document> collection) {
        List<String> listNIF = new ArrayList<>();
        int num = 0;
        try (MongoCursor<Document> cursor = collection.find().iterator()) {
            while (cursor.hasNext()) {
                num++;
                Document d = cursor.next();
                listNIF.add(d.getString("nif"));
            }
        }
        System.out.println("===" + num + " clients===: ");
        for (String nif : listNIF) {
            System.out.println(listNIF.indexOf(nif) + ". " + nif);
        }
        return listNIF;
    }

    public static void esborrarTot() {
        try (MongoClient mongoClient = MongoClients.create()) {
            MongoDatabase database = mongoClient.getDatabase("bd_prova");
            MongoCollection<Document> collection = database.getCollection("clients");

            DeleteResult deleteResult = collection.deleteMany(gt("total_facturacio", -999));
            System.out.println("S'ha esborrat " + deleteResult.getDeletedCount() + " clients");
        }
    }

    public static String stringIsNull() {
        Scanner sc = new Scanner(System.in);
        String n = sc.nextLine();
        if (n.length() == 0) {
            return null;
        } else {
            return n;
        }
    }
}
