package liling_act_2_3_JavaMongoDB;

import java.util.ArrayList;
import java.util.List;

public class Client {
    String nif;
    String nom;
    int total_facturacio;
    String telefon;
    String correu;
    List<Comanda> comandes;

    public Client(){

    }
    public Client(String nif, String nom, int total_facturacio, String telefon, String correu) {
        this.nif = nif;
        this.nom = nom;
        this.total_facturacio = total_facturacio;
        this.telefon = telefon;
        this.correu = correu;
        comandes = new ArrayList<>();
    }

    public Client(String nif, String nom, int total_facturacio, String telefon, String correu, List<Comanda> comandes) {
        this.nif = nif;
        this.nom = nom;
        this.total_facturacio = total_facturacio;
        this.telefon = telefon;
        this.correu = correu;
        this.comandes = comandes;
    }

    public Client(String nif, String nom, int total_facturacio) {
        this.nif = nif;
        this.nom = nom;
        this.total_facturacio = total_facturacio;
        comandes = new ArrayList<>();
    }

    @Override
    public String toString() {
        return "Client{" +
                "nif='" + nif + '\'' +
                ", nom='" + nom + '\'' +
                ", total_facturacio=" + total_facturacio +
                ", telefon='" + telefon + '\'' +
                ", correu='" + correu + '\'' +
                ", comandes=" + comandes +
                '}';
    }
}
