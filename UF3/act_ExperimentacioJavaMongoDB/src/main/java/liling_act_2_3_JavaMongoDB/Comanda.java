package liling_act_2_3_JavaMongoDB;



import java.util.Date;


public class Comanda {
    Date data_comanda;
    int imports;
    boolean pagada;

    public Comanda(Date data_comanda, int imports) {
        this.data_comanda = data_comanda;
        this.imports = imports;
        pagada=false;
    }

    public Comanda() {
    }

    @Override
    public String toString() {
        return "\n      Comanda{" +
                "data_comanda=" + data_comanda +
                ", imports=" + imports +
                ", pagada=" + pagada +
                '}';
    }
}
