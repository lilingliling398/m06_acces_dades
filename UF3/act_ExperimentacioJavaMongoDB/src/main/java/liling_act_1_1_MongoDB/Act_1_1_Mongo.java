package liling_act_1_1_MongoDB;

import com.mongodb.client.*;
import com.mongodb.client.result.DeleteResult;
import org.bson.Document;

import java.util.ArrayList;
import java.util.List;

import static com.mongodb.client.model.Filters.gt;

public class Act_1_1_Mongo {
    public static void inserir(Assignatura assignatura) {
        try (MongoClient mongoClient = MongoClients.create()) {
            MongoDatabase database = mongoClient.getDatabase("bd_prova");
            MongoCollection<Document> collection = database.getCollection("assignatures");

            Document document = new Document("nom", assignatura.nom).append("hores_setmana", assignatura.hores_setmana);
            collection.insertOne(document);
        }
    }

    public static void esborrarTot() {
        try (MongoClient mongoClient = MongoClients.create()) {
            MongoDatabase database = mongoClient.getDatabase("bd_prova");
            MongoCollection<Document> collection = database.getCollection("assignatures");

            DeleteResult deleteResult = collection.deleteMany(gt("hores_setmana", 0));
            System.out.println("S'ha esborrat " + deleteResult.getDeletedCount() + " assignatures");
        }
    }

    public static void llegirAssigHoraMinim(int horesMinim) {
        List<Assignatura> assignaturas = new ArrayList<>();
        try (MongoClient mongoClient = MongoClients.create()) {
            MongoDatabase database = mongoClient.getDatabase("bd_prova");
            MongoCollection<Document> collection = database.getCollection("assignatures");


            try (MongoCursor<Document> cursor = collection.find(gt("hores_setmana", horesMinim)).iterator()) {
                while (cursor.hasNext()) {
                    Document d = cursor.next();
                    String nom = d.getString("nom");
                    int hores = d.getInteger("hores_setmana");
                    assignaturas.add(new Assignatura(nom, hores));
                }
            }

        }
        System.out.println("Assignatures trobades amb hores > " + horesMinim + ":");
        for (Assignatura assignatura : assignaturas) {
            System.out.println("ASSIGNATURA: " + assignatura.nom + " HORES: " + assignatura.hores_setmana);
        }
    }


}
