
package examen_liling;

import com.mongodb.client.MongoClient;
import com.mongodb.client.MongoClients;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import org.bson.Document;


import Util.Leer;
import com.mongodb.client.*;
import com.mongodb.client.result.DeleteResult;
import org.bson.Document;

import javax.print.Doc;
import java.util.ArrayList;
import java.util.List;

import static com.mongodb.client.model.Filters.where;

public class ExUF3 {

    public static void main(String[] args) {
        NomComercial renault_clio = new NomComercial("Renault", "Clio");
        NomComercial bmw_golf = new NomComercial("BMW", "Golf");
        NomComercial liling = new NomComercial("liling", "Golf");
        Revisio r1 = new Revisio(2018, true, "Sense incidències");
        Revisio r2 = new Revisio(2019, true, "Sense incidències");
        Revisio r3 = new Revisio(2020, true, "Retrovisor dret amb mala visibilitat");
        Revisio r4 = new Revisio(2021, false, "Presió dels pneumàtics insuficient");

        Revisio r5 = new Revisio(2019, false, "Sense incidències");

        Cotxe c1 = new Cotxe("1234ABC", 120, renault_clio);
        c1.revisions.add(r1);
        c1.revisions.add(r2);
        Cotxe c2 = new Cotxe("5678DEF", 150, bmw_golf);
        c2.revisions.add(r2);
        c2.revisions.add(r3);
        c2.revisions.add(r4);

        Cotxe c3 = new Cotxe("5678LXL", 50, bmw_golf);
        c3.revisions.add(r2);
        c3.revisions.add(r4);
        c3.revisions.add(r4);


        Cotxe c4 = new Cotxe("asrfs", 105, bmw_golf);
        c4.revisions.add(r2);
        c4.revisions.add(r5);

        Cotxe c5 = new Cotxe("Nooo", 99, liling);
        c5.revisions.add(r2);
        c5.revisions.add(r5);
        



        List<Cotxe> llista_cotxes = exercici2("BMW");
        for (Cotxe c : llista_cotxes) {
            System.out.println(c);
        }
    }

    public static void exercici1(Cotxe cotxe) {
        /* TODO: Persistir el cotxe a la BBDD noSQL (Mongo) */

        try (MongoClient mongoClient = MongoClients.create()) {
            MongoDatabase database = mongoClient.getDatabase("bd_prova");
            MongoCollection<Document> collection = database.getCollection("cotxes");

            Document docCotxe = new Document();
            docCotxe.append("matricula", cotxe.matricula);
            docCotxe.append("cavalls", cotxe.cavalls);

            Document docNomComercial = new Document();
            docNomComercial.append("marca", cotxe.nom_comercial.marca);
            docNomComercial.append("model", cotxe.nom_comercial.model);
            docCotxe.append("nom_comercial", docNomComercial);

            List<Document> listRevisio = new ArrayList<>();
            for (Revisio revisio : cotxe.revisions) {
                listRevisio.add(new Document("any", revisio.any)
                        .append("favorable", revisio.favorable)
                        .append("comentaris", revisio.comentaris));
            }
            docCotxe.append("revisions", listRevisio);

            collection.insertOne(docCotxe);
            System.out.println("S'ha insertat el cotxe" + cotxe.matricula);
        }


    }

    public static List<Cotxe> exercici2(String marca) {
        List<Cotxe> llista_cotxes = new ArrayList<>();
        try (MongoClient mongoClient = MongoClients.create()) {
            MongoDatabase database = mongoClient.getDatabase("bd_prova");
            MongoCollection<Document> collection = database.getCollection("cotxes");

            try (MongoCursor<Document> cursor = collection.find(where("this.revisions.length>=3 && this.nom_comercial.marca=='" + marca+"'")).iterator()) {
                while (cursor.hasNext()){
                    Document d = cursor.next();

                    Document dNomComercial = (Document) d.get("nom_comercial");
                    NomComercial nomComercial = new NomComercial(dNomComercial.getString("marca"), dNomComercial.getString("model"));

                    Cotxe cotxe = new Cotxe(d.getString("matricula"), d.getInteger("cavalls"),nomComercial);

                    List<Document> documentList = d.getList("revisions", Document.class);
                    for (Document revisio : documentList) {
                        cotxe.revisions.add(new Revisio(revisio.getInteger("any"),
                                revisio.getBoolean("favorable"),
                                revisio.getString("comentaris")));
                    }
                    llista_cotxes.add(cotxe);
                }
            }
        }

        return llista_cotxes;
    }

}
