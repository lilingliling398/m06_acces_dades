package Util;

/*import java.sql.Date;
import java.time.DateTimeException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Scanner;*/

public class Leer {
    /*static Scanner sc = new Scanner(System.in);
    //pedirInt(max, min)
    //pedirInt()
    //pedirDouble()
    //pedirLocalDate()
    //pedirDate()

    public static int pedirInt(int min, int max) {
        int opcion = pedirInt();
        if (opcion >= min && opcion <= max) {
            return opcion;
        } else {
            System.out.print("Inrodueix un int entre " + min + "-" + max + ": ");
            opcion = pedirInt(min, max);
        }
        return opcion;
    }

    public static int pedirInt() {
        String valor = sc.nextLine();
        int resultat = 0;
        try {
            resultat = Integer.parseInt(valor);
        } catch (NumberFormatException e) {
            System.out.print("Introdueix un int: ");
            resultat = pedirInt();
        }
        return resultat;
    }


    public static double pedirDouble() {
        String valor = sc.nextLine();
        double resultat = 0;
        try {
            resultat = Double.parseDouble(valor);
        } catch (NumberFormatException e) {
            System.out.print("Introdueix un double: ");
            resultat = pedirDouble();
        }
        return resultat;
    }

    public static boolean pedirBoolean() {
        String valor = sc.nextLine();

        if (valor.toLowerCase().equals("s")) {
            return true;
        } else if (valor.toLowerCase().equals("n")) {
            return false;
        } else {
            System.out.print("Introdueix un 's' o 'n': ");
            return pedirBoolean();
        }
    }

    public static LocalDate pedirLocalData() {
        System.out.print("Introdueixi la data: ");
        LocalDate localDate;
        while (true) {
            String dataIntroduida = sc.nextLine();
            try {
                localDate = LocalDate.parse(dataIntroduida, DateTimeFormatter.ofPattern("dd/MM/yyyy"));
                break;
            } catch (DateTimeException e) {
                System.out.print("La data és incorrecta. El format es dd/mm/aaaa: ");
            }
        }
        return localDate;
    }

    public static Date pedirData() {
        System.out.print("Introdueix la data: ");
        Date data;
        while (true) {
            String dataIntroduida = sc.nextLine();
            try {
                LocalDate localDate = LocalDate.parse(dataIntroduida, DateTimeFormatter.ofPattern("dd/MM/yyyy"));
                data = Date.valueOf(localDate);
                break;
            } catch (DateTimeException e) {
                System.out.print("La data és incorrecta. El format es dd/MM/aaaa: ");
            }
        }
        return data;
    }

    //Menu estructura
    public static void main(String[] args) {
        //

        boolean acabarMenu = false;
        while (!acabarMenu) {

            imprimirMenu();

            System.out.print("Opcio?: ");
            int opcion = Util.Leer.pedirInt(0, 5);
            System.out.println();

            switch (opcion) {
                case 1:

                    bloquejarPantalla();
                    break;
                case 2:

                    bloquejarPantalla();
                    break;
                case 3:

                    bloquejarPantalla();
                    break;
                case 4:

                    bloquejarPantalla();
                    break;
                case 5:

                    bloquejarPantalla();
                    break;
                case 0:
                    acabarMenu = true;
                    break;
            }
        }
    }

    private static void imprimirMenu() {
        System.out.println("");
        System.out.println("-MENU----------------------------------------------------");
        System.out.println("    0. Sortir");
        System.out.println("    1. ");
        System.out.println("    2. ");
        System.out.println("    3. ");
        System.out.println("    4. ");
        System.out.println("    5. ");
    }

    public static void bloquejarPantalla() {
        Scanner sc = new Scanner(System.in);
        System.out.print("\n'C' per a continuar ");
        boolean valido = false;
        while (!valido) {
            String valor = sc.nextLine();
            if ("C".equals(valor) || "c".equals(valor)) {
                valido = true;
            }
        }
    }
    //menu*/

}


