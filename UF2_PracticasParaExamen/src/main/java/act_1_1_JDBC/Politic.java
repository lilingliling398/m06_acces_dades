package act_1_1_JDBC;

import java.sql.Date;

public class Politic {
    private String nif;
    private String nom;
    private Date dataNaixement;
    private int sou;
    private boolean esCorrupte;

    public Politic(String nif, String nom, Date dataNaixement, int sou, boolean esCorrupte) {
        this.nif = nif;
        this.nom = nom;
        this.dataNaixement = dataNaixement;
        this.sou = sou;
        this.esCorrupte = esCorrupte;
    }

    public Politic() {
    }

    @Override
    public String toString() {
        return "Politic{" +
                "nif='" + nif + '\'' +
                ", nom='" + nom + '\'' +
                ", dataNaixement=" + dataNaixement +
                ", sou=" + sou +
                ", esCorrupte=" + esCorrupte +
                '}';
    }

    public String getNif() {
        return nif;
    }

    public void setNif(String nif) {
        this.nif = nif;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public Date getDataNaixement() {
        return dataNaixement;
    }

    public void setDataNaixement(Date dataNaixement) {
        this.dataNaixement = dataNaixement;
    }

    public int getSou() {
        return sou;
    }

    public void setSou(int sou) {
        this.sou = sou;
    }

    public boolean isEsCorrupte() {
        return esCorrupte;
    }

    public void setEsCorrupte(boolean esCorrupte) {
        this.esCorrupte = esCorrupte;
    }
}
