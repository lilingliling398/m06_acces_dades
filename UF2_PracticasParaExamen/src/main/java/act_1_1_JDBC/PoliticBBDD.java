package act_1_1_JDBC;

import java.io.Closeable;
import java.io.IOException;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;


public class PoliticBBDD implements Closeable {
    private Connection con;

    public PoliticBBDD(){
        String url = "jdbc:mysql://localhost/lamevabbdd";//jdbc:mysql://localhost/lamevabbdd";
        String usuari = "liling";
        String password = "jupiter";
        try {
            con = DriverManager.getConnection(url, usuari, password);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void close() throws IOException {
        try {
            con.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }


    public void crearTaula() {
        try {
            Statement statement = con.createStatement();

            String sentencia = "create table Proba_Politic (" +
                    "nif char(9), " +
                    "nom varchar(50), " +
                    "dataNaixement date, " +
                    "sou int,"+
                    "esCorrupte enum('s','n')," +
                    "PRIMARY KEY (nif)" +
                    ");";
            statement.execute(sentencia);
            System.out.println("Taula creat correctament");
        } catch (SQLException e) {
            e.printStackTrace();
        }


    }

    public void borrarTaula() {
        try {
            Statement statement = con.createStatement();

            String sentencia = "drop table if exists Proba_Politic;";
            statement.execute(sentencia);
            System.out.println("Taula borrar correctament");
        }catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void guardarPolitic(Politic politic) {
        try {
            String sentencia = "insert into Proba_Politic values(?,?,?,?,?);";
            PreparedStatement preparedStatement = con.prepareStatement(sentencia);
            preparedStatement.setString(1,politic.getNif());
            preparedStatement.setString(2, politic.getNom());
            preparedStatement.setDate(3, politic.getDataNaixement());
            preparedStatement.setInt(4, politic.getSou());
            preparedStatement.setString(5, politic.isEsCorrupte() ? "s" : "n");

            preparedStatement.executeUpdate();
            System.out.println("Dades insertades correctament");
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void leerTaula() {
        try {
            Statement statement = con.createStatement();
            String sentencia = "Select * from Proba_Politic;";
            statement.execute(sentencia);

            ResultSet resultSet = statement.executeQuery(sentencia);

            List<Politic> politicList = new ArrayList<>();
            while (resultSet.next()){
                String nif = resultSet.getString("nif");
                String nom = resultSet.getString("nom");
                Date data = resultSet.getDate("dataNaixement");
                int  sou = resultSet.getInt("sou");
                boolean corrupte = resultSet.getString("esCorrupte").equals("s");

                politicList.add(new Politic(nif, nom, data, sou, corrupte));
            }

            for(Politic politic : politicList) {
                System.out.println(politic);
            }

        }catch (SQLException e){
            e.printStackTrace();
        }

    }








}
