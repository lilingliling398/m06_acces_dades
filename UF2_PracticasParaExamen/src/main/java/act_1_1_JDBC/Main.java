package act_1_1_JDBC;

import java.sql.Date;

public class Main {
    public static void main(String[] args) {
        PoliticBBDD bbdd = new PoliticBBDD();

        //bbdd.borrarTaula();
        //bbdd.crearTaula();
        //aaaa-MM-dd
        Politic politic = new Politic("12345678a", "nombrea", Date.valueOf("2020-10-10") , 1000, true);
        bbdd.guardarPolitic(politic);
        bbdd.leerTaula();
    }
}
