package act_1_3_JDBC_Extension;

import Util.Leer;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class SqlBBDD {
    private Connection con;

    public SqlBBDD() {
        String url = "jdbc:mysql://localhost/lamevabbdd";//jdbc:mysql://localhost/lamevabbdd";
        String usuari = "liling";
        String password = "jupiter";
        try {
            con = DriverManager.getConnection(url, usuari, password);
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    public void crearTaula() {
        try (Statement statement = con.createStatement()) {
            String sentencia = "create table Proba_Client (" +
                    "dni int(8)," +
                    "nom varchar(30)," +
                    "premium enum('s','n')," +
                    "PRIMARY KEY (dni) " +
                    ");";
            statement.execute(sentencia);
            System.out.println("Taula ProbaClient creat correctament");
        } catch (SQLException e) {
            System.out.println("No s'ha pogut crear la taubla Client, perque ja existeix");
        }

        try (Statement statement = con.createStatement();) {
            String sentencia = "create table Proba_Comanda (" +
                    "numComanda int(8)," +
                    "preuTotal double(9,2)," +
                    "data date," +
                    "dniClient int(8), " +
                    "primary key (numComanda), " +
                    "foreign key (dniClient) references Proba_Client (dni)" +
                    ");";
            statement.execute(sentencia);
            System.out.println("Taula ProbaClient creat correctament");
        } catch (SQLException e) {
            System.out.println("No s'ha pogut crear la taubla Comanda, perque ja existeix");
        }
    }

    public List<Client> recuperarBBDD() throws SQLException {
        List<Client> clientList = new ArrayList<>();

        //para cada resulset crear un statement
        Statement statement = con.createStatement();
        Statement statement1 = con.createStatement();

        String sentencia = "select * from Proba_Client;";
        ResultSet resultSet = statement.executeQuery(sentencia);
        while (resultSet.next()) {
            int dni = resultSet.getInt("dni");
            String nom = resultSet.getString("nom");
            boolean premium = resultSet.getString("premium").equals("s");
            Client client = new Client(dni, nom, premium);

            String sentenciaComanda = "select * from Proba_Comanda where dniClient=" + dni + ";";
            ResultSet resultSetComanda = statement1.executeQuery(sentenciaComanda);
            while (resultSetComanda.next()) {
                int numComanda = resultSetComanda.getInt("numComanda");
                double preuTotal = resultSetComanda.getDouble("preuTotal");
                Date date = resultSetComanda.getDate("data");
                int dniClient = resultSetComanda.getInt("dniClient");
                client.getComandas().add(new Comanda(numComanda, preuTotal, date, dniClient));
            }
            clientList.add(client);

        }
        return clientList;
    }

    public void emmagarBBDD(List<Client> clientList) throws SQLException {
        Statement statement = con.createStatement();

        String delete = "delete from Proba_Comanda;";
        statement.execute(delete);
        delete = "delete from Proba_Client;";
        statement.execute(delete);

        for (Client client : clientList) {
            String insertClient = "insert into Proba_Client values (?,?,?);";
            PreparedStatement preparedStatement = con.prepareStatement(insertClient);
            preparedStatement.setInt(1, client.getDni());
            preparedStatement.setString(2, client.getNom());
            preparedStatement.setString(3, client.isPremium() ? "s" : "n");
            preparedStatement.executeUpdate();
            for (Comanda comanda : client.getComandas()) {
                String insertComanda = "insert into Proba_Comanda values (?,?,?,?);";
                PreparedStatement preparedStatement1 = con.prepareStatement(insertComanda);
                preparedStatement1.setInt(1, comanda.getNum_comanda());
                preparedStatement1.setDouble(2, comanda.getPreu_total());
                preparedStatement1.setDate(3, comanda.getData());
                preparedStatement1.setInt(4, comanda.getDni_client());
                preparedStatement1.executeUpdate();
            }
        }
    }

    public void altaClient(List<Client> clientList) throws SQLException {
        Scanner sc = new Scanner(System.in);
        boolean darAlta;
        int dni;
        do {
            System.out.println("Donar alta client dni: ");
            dni = sc.nextInt();

            darAlta = true;
            for (Client client : clientList) {
                if (client.getDni() == dni) {
                    darAlta = false;
                    System.out.println("Ya esta registrat aquest client");
                    break;
                }
            }
        } while (!darAlta);

        String insertClient = "insert into Proba_Client values (?,?,?)";
        PreparedStatement preparedStatement = con.prepareStatement(insertClient);
        preparedStatement.setInt(1, dni);
        preparedStatement.setString(2, "lilingAlta");
        preparedStatement.setString(3, "s");
        preparedStatement.executeUpdate();


        clientList.add(new Client(dni, "lilingAlta", true));
    }

    public void altaComanda(List<Client> clientList) throws SQLException {
        int numComada;

        for (Client client :clientList) {
            System.out.printf("%d) %d - %s\n", clientList.indexOf(client)+1, client.getDni(), client.getNom());
        }
        System.out.println("client?: ");
        int index = Leer.opcionInt(1, clientList.size());


        Client client = clientList.get(index-1);
        boolean alta;
        do {
            alta = true;
            System.out.println("Alta comanda amb numComanda: ");
            numComada = Leer.pedirInt();


            for (Client client1 : clientList) {
                for (Comanda comanda : client1.getComandas()) {
                    if (numComada == comanda.getNum_comanda()) {
                        alta = false;
                        System.out.println("Ya esta registrat aquest comanda");
                        break;
                    }
                }
            }
        }while (!alta);


        String insertClient = "insert into Proba_Comanda values (?,?,?,?)";
        PreparedStatement preparedStatement = con.prepareStatement(insertClient);
        preparedStatement.setInt(1, numComada);
        preparedStatement.setDouble(2, 100.55);
        preparedStatement.setDate(3, Date.valueOf("2020-10-10"));
        preparedStatement.setInt(4, client.getDni());
        preparedStatement.executeUpdate();

        client.getComandas().add(new Comanda(numComada, 100.55, Date.valueOf("2020-10-10"), client.getDni()));
    }

    public void eliminarClient(List<Client> clientList) throws SQLException {
        Statement statement = con.createStatement();
        for (Client client :clientList) {
            System.out.printf("%d) %d - %s\n", clientList.indexOf(client)+1, client.getDni(), client.getNom());
        }
        System.out.println("client?: ");
        int index = Leer.opcionInt(1, clientList.size());

        Client client = clientList.get(index-1);

        String sentencia = "delete from Proba_Comanda where dniClient="+client.getDni();
        statement.execute(sentencia);
        sentencia = "delete from Proba_Client where dni="+client.getDni();
        statement.execute(sentencia);

        clientList.remove(client);

    }

    public void actualixarClient(List<Client> clientList) throws SQLException {
        for (Client client :clientList) {
            System.out.printf("%d) %d - %s\n", clientList.indexOf(client)+1, client.getDni(), client.getNom());
        }
        System.out.println("client?: ");
        int index = Leer.opcionInt(1, clientList.size());

        Client client = clientList.get(index-1);

        Scanner sc = new Scanner(System.in);
        System.out.println("DADES NOU DEL CLIENT");
        System.out.println("Nom: ");
        String nom = sc.nextLine();

        System.out.println("Premium: ");
        boolean premium = Leer.pedirBoolean();

        String actualizar = "update Proba_Client set nom=?, premium=? where dni=?;";
        PreparedStatement preparedStatement = con.prepareStatement(actualizar);
        preparedStatement.setString(1, nom);
        preparedStatement.setString(2, premium ? "s" : "n");
        preparedStatement.setInt(3, client.getDni());
        preparedStatement.executeUpdate();

        client.setNom(nom);
        client.setPremium(premium);
    }

    public void busqueda() throws SQLException {
        Scanner sc = new Scanner(System.in);
        System.out.println("Buscar: ");
        String busqueda = sc.nextLine();

        Statement statement = con.createStatement();

        String sentencia = "select nom from Proba_Client where nom like '"+ busqueda +"%';";
        ResultSet resultSet = statement.executeQuery(sentencia);
        while (resultSet.next()){
            System.out.println(resultSet.getString("nom"));
        }


    }


}
