package act_1_3_JDBC_Extension;

import Util.Leer;

import java.sql.SQLException;
import java.util.List;

public class Main {
    public static void main(String[] args) throws SQLException {
        /*SqlBBDD sqlBBDD = new SqlBBDD();

        List<Client> clients = new ArrayList<>();
        clients.add(new Client(12345677, "Bryan", true));
        clients.add(new Client(12345678, "Liling", true));
        clients.add(new Client(12345679, "Byron", true));

        clients.get(0).getComandas().add(new Comanda(1, 10.50, Date.valueOf("2021-01-15"), 12345677));
        clients.get(0).getComandas().add(new Comanda(2, 10.50, Date.valueOf("2021-01-15"), 12345677));
        clients.get(1).getComandas().add(new Comanda(3, 10.50, Date.valueOf("2021-01-15"), 12345678));

        *//*sqlBBDD.crearTaula();

        sqlBBDD.emmagarBBDD(clients);*//*

        List<Client> listclient = sqlBBDD.recuperarBBDD();
        for(Client client : listclient) {
            System.out.println(client);
        }

        sqlBBDD.altaClient(listclient);

        sqlBBDD.altaComanda(listclient);
        */



        SqlBBDD sqlBBDD = new SqlBBDD();
        List<Client> clientList = sqlBBDD.recuperarBBDD();

        while (true) {
            imprimirMenu();
            System.out.println("opcio:");
            switch (Leer.opcionInt(0, 5)) {
                case 1:
                    sqlBBDD.eliminarClient(clientList);
                    break;
                case 2:
                    sqlBBDD.actualixarClient(clientList);
                    break;
                case 3:
                    sqlBBDD.busqueda();
                    break;
                case 4:
                    sqlBBDD.altaClient(clientList);
                    break;
                case 5:
                    sqlBBDD.altaComanda(clientList);
                    break;

                case 0:
                    System.out.println("Adios");
                    break;
            }
        }
    }
    private static void imprimirMenu() {
        System.out.println("");
        System.out.println("__MENU__________________________________________");
        System.out.println("1. Eliminació d'un client");
        System.out.println("2. Actualitzar dades d'un client");
        System.out.println("3. Cerca de nom");
        System.out.println("4. Alta d'un nou client");
        System.out.println("5. Alta d'una nova comanda");
        System.out.println(". Mostrar per pantalla les comandes d'un client");
        System.out.println(". Generació de resum de facturació");

    }

}
