package act_1_2_JDBC;

import Util.Leer;

import java.sql.Date;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class Main {
    public static void main(String[] args) throws SQLException {
        /*SqlBBDD sqlBBDD = new SqlBBDD();

        List<Client> clients = new ArrayList<>();
        clients.add(new Client(12345677, "Bryan", true));
        clients.add(new Client(12345678, "Liling", true));
        clients.add(new Client(12345679, "Byron", true));

        clients.get(0).getComandas().add(new Comanda(1, 10.50, Date.valueOf("2021-01-15"), 12345677));
        clients.get(0).getComandas().add(new Comanda(2, 10.50, Date.valueOf("2021-01-15"), 12345677));
        clients.get(1).getComandas().add(new Comanda(3, 10.50, Date.valueOf("2021-01-15"), 12345678));

        *//*sqlBBDD.crearTaula();

        sqlBBDD.emmagarBBDD(clients);*//*

        List<Client> listclient = sqlBBDD.recuperarBBDD();
        for(Client client : listclient) {
            System.out.println(client);
        }

        sqlBBDD.altaClient(listclient);

        sqlBBDD.altaComanda(listclient);
        */



        SqlBBDD sqlBBDD = new SqlBBDD();
        List<Client> clientList = sqlBBDD.recuperarBBDD();

        while (true) {
            switch (Leer.opcionInt(1, 4)) {
                case 1:
                    sqlBBDD.altaClient(clientList);
                    break;
                case 2:
                    sqlBBDD.altaComanda(clientList);
                    break;
                case 3:
                    sqlBBDD.emmagarBBDD(clientList);
                    break;
                case 4:
                    clientList = sqlBBDD.recuperarBBDD();
                    for (Client client : clientList) {
                        System.out.println(client);
                    }
                    break;
                case 0:
                    System.out.println("Adios");
                    break;
            }
        }
    }

}
