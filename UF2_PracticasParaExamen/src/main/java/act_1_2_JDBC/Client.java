package act_1_2_JDBC;

import java.util.ArrayList;
import java.util.List;

public class Client {
    private int dni;
    private String nom;
    private boolean premium;
    private List<Comanda> comandas;

    public Client(int dni, String nom, boolean premium) {
        this.dni = dni;
        this.nom = nom;
        this.premium = premium;
        comandas = new ArrayList<>();
    }

    public Client(){
        comandas = new ArrayList<>();
    }

    @Override
    public String toString() {
        return "Client{" +
                "dni=" + dni +
                ", nom='" + nom + '\'' +
                ", premium=" + premium +
                ", Comandas=" + comandas +

                '}';

    }

    public int getDni() {
        return dni;
    }

    public void setDni(int dni) {
        this.dni = dni;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public boolean isPremium() {
        return premium;
    }

    public void setPremium(boolean premium) {
        this.premium = premium;
    }

    public List<Comanda> getComandas() {
        return comandas;
    }

    public void setComandas(List<Comanda> comandas) {
        this.comandas = comandas;
    }
}
