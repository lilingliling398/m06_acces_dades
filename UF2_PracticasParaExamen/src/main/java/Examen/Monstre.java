package Examen;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "Montre")
public class Monstre {
    @Id
    @Column(name = "id_monstre")
    int id_monstre;

    @Column(name = "nom")
    String nom;

    @Column(name = "atac")
    int atac;

    @Column(name = "id_nivell")
    int id_nivell;

    public Monstre(int id_monstre, String nom, int atac, int id_nivell) {
        this.id_monstre = id_monstre;
        this.nom = nom;
        this.atac = atac;
        this.id_nivell = id_nivell;
    }

    public Monstre() {
    }

    @Override
    public String toString() {
        return "Monstre{" +
                "id_monstre=" + id_monstre +
                ", nom='" + nom + '\'' +
                ", atac=" + atac +
                ", id_nivell=" + id_nivell +
                '}';
    }
}
