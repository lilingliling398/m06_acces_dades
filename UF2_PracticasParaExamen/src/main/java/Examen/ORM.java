package Examen;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.hibernate.query.Query;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

public class ORM {

    //4
    public static void guardarNivell(Nivell nivell) {
        SessionFactory factory = new Configuration().configure("hibernate.cfg.xml").addAnnotatedClass(Nivell.class).buildSessionFactory();
        Session session = factory.openSession();

        try {
            session.beginTransaction();
            session.save(nivell);
            session.getTransaction().commit();
            System.out.println("Client guardat correctamnet");

        } finally {
            session.close();
            factory.close();
        }
    }

    public static List<Nivell> act5(int dificultat_max) {
        List<Nivell> nivellList;

        SessionFactory factory = new Configuration().configure("hibernate.cfg.xml").addAnnotatedClass(Nivell.class).buildSessionFactory();
        Session session = factory.openSession();

        try {
            String setencia = "from Nivell N where N.dificultat =" + dificultat_max + " or N.dificultat < " + dificultat_max;
            Query query = session.createQuery(setencia);
            nivellList = query.list();

        } finally {
            session.close();
            factory.close();
        }

        return nivellList;
    }

    //6
    public static void guardarNivellJDBC(Nivell nivell) throws SQLException {
        String url = "jdbc:mysql://localhost/lamevabbdd";
        String usuari = "root";
        String password = "Jupiter1!";
        Connection con = null;
        try {
            con = DriverManager.getConnection(url, usuari, password);

            String sentencia = "insert into Nivell values (?,?,?);";
            PreparedStatement preparedStatement = con.prepareStatement(sentencia);
            preparedStatement.setInt(1, nivell.id_nivell);
            preparedStatement.setString(2, nivell.nom);
            preparedStatement.setInt(3, nivell.dificultat);
            preparedStatement.executeUpdate();
            System.out.println("Nivell guardat correctament");
        } finally {
            assert con != null;
            con.close();
        }
    }

    public static List<Monstre> act7(int atac_min) throws SQLException {
        String url = "jdbc:mysql://localhost/lamevabbdd";
        String usuari = "root";
        String password = "Jupiter1!";
        Connection con = null;
        List<Monstre> monstres = new ArrayList<>();

        try {
            con = DriverManager.getConnection(url, usuari, password);

            Statement statement = con.createStatement();
            String sentencia = "select * from Monstre where atac>=" + atac_min + ";";
            ResultSet resultSet = statement.executeQuery(sentencia);
            while (resultSet.next()) {
                int idMonstre = resultSet.getInt("id_monstre");
                String nom= resultSet.getString("nom");
                int atac = resultSet.getInt("atac");
                int idNivell = resultSet.getInt("id_nivell");

                monstres.add(new Monstre(idMonstre, nom, atac, idNivell));
            }

        } finally {
            assert con != null;
            con.close();
        }
        return monstres;
    }
}
