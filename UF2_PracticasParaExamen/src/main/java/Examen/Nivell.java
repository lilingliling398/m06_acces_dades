package Examen;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "Nivell")
public class Nivell {
    @Id
    @Column(name = "id_nivell")
    int id_nivell;

    @Column(name = "nom")
    String nom;

    @Column(name = "dificultat")
    int dificultat;

    public Nivell(int id_nivell, String nom, int dificultat) {
        this.id_nivell = id_nivell;
        this.nom = nom;
        this.dificultat = dificultat;
    }

    public Nivell() {
    }

    @Override
    public String toString() {
        return "Nivell{" +
                "id_nivell=" + id_nivell +
                ", nom='" + nom + '\'' +
                ", dificultat=" + dificultat +
                '}';
    }
}

