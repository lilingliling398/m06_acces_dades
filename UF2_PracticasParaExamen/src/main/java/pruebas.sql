create table nivell(
    id_nivell int,
    nom varchar(50),
    dificultat int,
    PRIMARY KEY (id_nivell)
    );

create table monstre(
    id_monstre int,
    nom varchar(50),
    atac int,
    id_nivell int,
    PRIMARY KEY (id_monstre),
    FOREIGN KEY (id_nivell) references nivell(id_nivell)
);
