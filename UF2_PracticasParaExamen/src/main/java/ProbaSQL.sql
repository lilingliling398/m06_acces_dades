CREATE DEFINER=`root`@`localhost` PROCEDURE `p_crear_resum_facturacio`(in p_mes INT(2), p_any INT(4))

BEGIN
-- declarar variables
DECLARE v_dniClient INT(8);
DECLARE v_preuTotal double(9,2);
-- declarar boolean para salir dell bucle
DECLARE acaba INT DEFAULT FALSE;

-- declarar query
DECLARE c_preu CURSOR FOR
	select dniClient, sum(preuTotal) from proba_comanda where dniClient
    in (select distinct dniClient from proba_comanda)
	and MONTH(Data)=p_mes AND YEAR(Data)=p_any
	group by dniClient;
-- declarar handler
DECLARE CONTINUE HANDLER FOR NOT FOUND SET acaba = TRUE;

CREATE TABLE IF NOT EXISTS Proba_Resum_Facturacio(
	mes INT(2),
	any INT(4),
	dniClient INT(8),
	quantitat DOUBLE(9,2),
	constraint PK_Proba_Resum_Facturacio PRIMARY KEY (mes, any, dniClient),
	foreign key (dniClient) references proba_client(dni)
);

DELETE from Proba_Resum_Facturacio where mes=p_mes AND any=p_any;

-- lista clientes
OPEN c_preu;
	read_loop: LOOP
		FETCH c_preu INTO v_dniClient, v_preuTotal;
		IF acaba THEN
			LEAVE read_loop;
		END IF;
	-- insertar un resum
     insert into proba_resum_facturacio values (p_mes, p_any, v_dniClient, v_preuTotal);
	END LOOP;
CLOSE c_preu;
END
















