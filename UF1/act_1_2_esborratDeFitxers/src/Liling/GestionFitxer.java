package Liling;

import Util.Leer;

import java.io.File;
import java.io.IOException;
import java.util.Scanner;

public class GestionFitxer {
    static Scanner sc = new Scanner(System.in);

    public static void crearFitxer(String ruta){
        System.out.print("Numero de fitxer que vols crear: ");
        int num = Leer.stringToInt();

        for (int i = 1; i < num + 1; i++) {

            File fitxerNou = new File(ruta + "\\file" + i + ".txt");

            if (!fitxerNou.exists()) {
                crearNouFitxer(fitxerNou);
            } else {
                System.out.println("Fitxer '" + fitxerNou.getName() + "' ya existeix");
            }

        }
    }

    //comprorar si el directorio exista y que no este vacio para borralo perfectamente

    //ACTIVITAT:
    // recorrer la lista de del directorio correspondiente y borrar la lista
    //finalmente borrar el dirrectorio (no se puede borrar un directorio con archivos dentro)
    public static void esborrarDirectori(String ruta){
        File directori = new File(ruta);

        if (directori.exists()) {
            if (directori.listFiles().length == 0) {
                System.out.println("El directori esta buit");
            } else {
                for (File file : directori.listFiles()) {
                    esborrar(file);
                }
                esborrar(directori);
            }
        } else {
            System.out.println("No existeix la ruta '" + ruta+"'");
            crearDir(directori);
        }


    }

    public static void crearNouFitxer(File fitxerNou) {
        try {
            if (fitxerNou.createNewFile()) {
                System.out.println("Fitxer '" + fitxerNou.getName() + "' creat");
            } else {
                System.out.println("Fitxer '" + fitxerNou.getName() + "' no s'ha creat correctament");
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void crearDir(File directori) {
        if (directori.mkdir()) {
            System.out.println("Directori '" + directori.getName() + "' creat");
        } else {
            System.out.println("Directori '" + directori.getName() + "' no s'ha creat correctamet");
        }
    }

    //borrar fitcher o directori
    public static void esborrar(File file) {
        if (file.delete()) {
            System.out.println("S'ha esborrat '" + file.getName() + "' ");
        } else {
            System.out.println("No ha esborrat '" + file.getName() + "' correctament");
        }
    }
}
