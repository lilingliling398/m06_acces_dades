package Liling;

import Util.Leer;

import java.io.IOException;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) throws IOException {
        Scanner sc = new Scanner(System.in);
        String ruta = "D:\\Directorio\\Ficheros";
        boolean seguirBucle;
        do {
            seguirBucle = true;
            System.out.println("");
            System.out.println("__Activitat 1.1_____________________________");
            System.out.println("    MENU");
            System.out.println("    1. Esborrar tot d'un directori");
            System.out.println("    2. Crear x fitxer especificat");
            System.out.print("Opcion?:");
            int opcion = Leer.opcionInt(0,2);

            switch (opcion) {
                case 1:
                    System.out.println("");
                    System.out.println("==ESBORRAR TOTES LES FITXERS D'UN DIRECTORI Y EL PROPI DIRECTORI==");
                    GestionFitxer.esborrarDirectori(ruta);
                    break;
                case 2:
                    System.out.println("");
                    System.out.println("==CREAR FITXER ESPECIFICAT==");
                    GestionFitxer.crearFitxer(ruta);
                    break;
                case 0:
                    seguirBucle = false;
                    break;
            }
        } while (seguirBucle);
    }
}
