package Util;

import java.util.Scanner;

public class Leer {
    static Scanner sc = new Scanner(System.in);

    public static int stringToInt() {
        String valor = sc.nextLine();
        int resultat = 0;
        try {
            resultat = Integer.parseInt(valor);
        } catch (NumberFormatException e) {
            System.out.print("Introdueix un int: ");
            resultat = stringToInt();
        }
        return resultat;
    }

    public static int opcionInt(int min, int max) {
        int opcion = stringToInt();
        if (opcion >= min && opcion <= max) {
            return opcion;
        } else {
            System.out.print("Introdueix un int entre " + min + "-" + max + ": ");
            opcion = opcionInt(min, max);
        }
        return opcion;
    }
}


