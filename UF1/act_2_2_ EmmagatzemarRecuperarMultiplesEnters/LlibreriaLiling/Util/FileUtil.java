package Util;

import java.io.Closeable;
import java.io.IOException;

public class FileUtil {
    public static void intentarTancar(Closeable aTancar){
        try {
            if (aTancar != null) {aTancar.close();}
        } catch (IOException ex) {ex.printStackTrace(System.err);}
    }

}
