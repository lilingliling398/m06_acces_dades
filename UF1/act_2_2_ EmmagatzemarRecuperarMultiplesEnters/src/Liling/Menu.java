package Liling;

import Util.Leer;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Menu {
    public static void main(String[] args) throws IOException {
        String ruta = "D:\\DirectorioA\\textoInt.txt";
        List<Integer> integerList = new ArrayList<>();

        boolean acabarMenu = false;
        while (!acabarMenu) {
            imprimirMenu();
            System.out.print("Opcio?: ");
            int opcion = Leer.opcionInt(0, 5);

            switch (opcion) {
                case 1:
                    Metode.escriureEnter(ruta, integerList);
                    bloquejarPantalla();
                    break;
                case 2:
                    Metode.llegirEnter(ruta, integerList);
                    bloquejarPantalla();
                    break;
                case 0:
                    acabarMenu = true;
                    break;
            }
        }
    }

    private static void imprimirMenu() {
        System.out.println("");
        System.out.println("-MENU----------------------------------------------------");
        System.out.println("    0. Sortir");
        System.out.println("    1. Escriure enters");
        System.out.println("    2. Llegir enters");

    }

    public static void bloquejarPantalla() {
        Scanner sc = new Scanner(System.in);
        System.out.print("\n'C' per a continuar ");
        boolean valido = false;
        while (!valido) {
            String valor = sc.nextLine();
            if ("C".equals(valor) || "c".equals(valor)) {
                valido= true;
            }
        }
    }
}
