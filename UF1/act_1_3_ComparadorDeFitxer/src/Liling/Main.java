package Liling;
//22/11/2020

import java.io.File;

public class Main {

    public static void main(String[] args) {
        System.out.println("Activitat 1.3. Comparador de fitxers\n");
        String rutaA = "D:\\DirectorioA";
        String rutaB = "D:\\DirectorioB";

        comparador(rutaA, rutaB);
    }

    private static void comparador(String rutaA, String rutaB) {
        File dirA = new File(rutaA);
        File dirB = new File(rutaB);

        if (dirA.exists() && dirB.exists()) {
            System.out.println("==S’està comparant els següents dos directoris");
            System.out.println("==== Directori A: " + dirA.getAbsolutePath());
            System.out.println("==== Directori B: " + dirB.getAbsolutePath());


            for (File fileA : dirA.listFiles()) {
                boolean existeFile = false;

                for (File fileB : dirB.listFiles()) {
                    if (fileA.getName().equals(fileB.getName())) {
                        existeFile = true;
                        comparadorFitxers(fileA, fileB);
                    }
                }

                if (!existeFile) {
                    System.out.println("- El fitxer '"+fileA.getName()+"' existeix al directori A, però no existeix al directori B");
                }
            }
        } else {
            if (!dirA.exists()) {
                System.out.println("No existeix el directori '" + dirA.getName() + "' ");
            }
            if (!dirB.exists()) {
                System.out.println("No existeix el directori '" + dirB.getName() + "' ");
            }
        }


    }

    private static void comparadorFitxers(File fileA, File fileB) {

        System.out.println("- El fitxer '" + fileA.getName() + "' existeix a ambdós directoris");
        if (fileA.lastModified() > fileB.lastModified()) {
            System.out.println("---- El fitxer al directori A és més nou (segons la data de modificació) que a B");
        }else if (fileA.lastModified() < fileB.lastModified()) {
            System.out.println("---- El fitxer al directori B és més nou que a A");
        }else {
            System.out.println("---- Tenen la mateixa data");
        }

        if (fileA.length() > fileB.length()) {
            System.out.println("---- El fitxer al directori A és més gran que a B");
        }else if (fileA.length() < fileB.length()) {
            System.out.println("---- El fitxer al directori B és més gran que a A");
        }else {
            System.out.println("---- Tenen la mateixa mida");
        }
    }


}
