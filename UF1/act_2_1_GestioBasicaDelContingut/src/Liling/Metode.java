package Liling;

import java.io.*;
import java.nio.charset.StandardCharsets;

public class Metode {

    //Fes un únic programa que emmagatzemi en un fitxer, per bytes, i posteriorment recuperi i pinti per pantalla les següents dades una darrera l'altra:
//    1.Un booleà
//    2.Un enter (superior a 100000)
//    3.Un conjunt de caràcters
    public static void escribirPorByte(String ruta) throws IOException {
        DataOutputStream streamEscriptura = new DataOutputStream(new BufferedOutputStream(new FileOutputStream(ruta)));

        try {
            streamEscriptura.writeBoolean(true);
            streamEscriptura.writeInt(123456);
            streamEscriptura.writeUTF("fsd");
        } finally {
            streamEscriptura.close();
        }
    }

    public static void leerPorByte(String ruta) throws IOException {
        DataInputStream streamLectura = new DataInputStream(new BufferedInputStream(new FileInputStream(ruta)));

        System.out.println("");
        System.out.println("--Dades de l'arxiu per bytes--");
        try {
            System.out.println(streamLectura.readBoolean());
            System.out.println(streamLectura.readInt());
            System.out.println(streamLectura.readUTF());
        } finally {
            streamLectura.close();
        }
    }

    //Fes un programa que emmagatzemi en un fitxer, per caràcters i en codificació UTF-8, i posteriorment recuperi i pinti per pantalla les següents dades una darrera l'altra:
    //1.Un enter
    //2.Una cadena de text que contingui caràcters estranys (ç, ñ, accents, dièresis...). Realitza la prova amb diferents codificacions.
    public static void escribirPorCaracter(String ruta) throws IOException {
        OutputStreamWriter streamWriter = new OutputStreamWriter(new FileOutputStream(ruta), StandardCharsets.ISO_8859_1);

        try {
            streamWriter.write(142);
            streamWriter.write("132");
            streamWriter.write("çàéïñabc");
            streamWriter.write("Text de prova");
        } finally {
            streamWriter.close();
        }

    }

    public static void leerPorCaracter(String ruta) throws IOException {
        InputStreamReader streamReader = new InputStreamReader(new FileInputStream(ruta), StandardCharsets.ISO_8859_1);

        System.out.println("");
        System.out.println("--Dades de l'arxiu per caràcters i en codificació UTF-8--");
        try {
            int charsLlegits;
            while ((charsLlegits = streamReader.read()) != -1) {
                System.out.print((char) charsLlegits);
            }
        } finally {
            streamReader.close();
        }
    }

}
