package Liling;

import Util.Leer;

import java.io.*;
import java.util.Scanner;

public class Menu {
    public static void main(String[] args) throws IOException {

        boolean acabarMenu = false;
        while (!acabarMenu) {

            imprimirMenu();
            System.out.print("Opcio?: ");
            int opcion = Leer.opcionInt(0,3);
            switch (opcion) {
                case 1:
                    String ruta = "D:\\DirectorioA\\texto.txt";
                    Metode.escribirPorByte(ruta);
                    Metode.leerPorByte(ruta);
                    bloquejarPantalla();
                    break;
                case 2:
                    String ruta2 = "D:\\DirectorioA\\texto2.txt";
                    Metode.escribirPorCaracter(ruta2);
                    Metode.leerPorCaracter(ruta2);
                    bloquejarPantalla();
                    break;
                case 3:

                    bloquejarPantalla();
                    break;
                case 0:
                    acabarMenu = true;
                    break;
            }
        }
    }

    public static void bloquejarPantalla() {
        Scanner sc = new Scanner(System.in);
        System.out.print("\n'C' per a continuar ");
        boolean valido = false;
        while (!valido) {
            String valor = sc.nextLine();
            if ("C".equals(valor) || "c".equals(valor)) {
                valido= true;
            }
        }
    }

    private static void imprimirMenu() {
        System.out.println("");
        System.out.println("-MENU----------------------------------------------------");
        System.out.println("    0. Sortir");
        System.out.println("    1. Emmagatzemar en un fitxer, per bytes");
        System.out.println("    2. Emmagatzemar en un fitxer, per caracters");
        System.out.println("    3. Afegir sense esborrar");

    }



}
