package Lilling;

import org.xml.sax.SAXException;


import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.xpath.XPathExpressionException;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Menu {

    public static void main(String[] args) throws IOException, SAXException, ParserConfigurationException, XPathExpressionException, TransformerException {
        Scanner sc = new Scanner(System.in);
        boolean repetir = true;
        List<Assignatura> assignaturas = new ArrayList<>();

        while (repetir) {
            imprimirMenu();
            boolean opcioValida = false;
            int num = 0;

            while (!opcioValida) {
                System.out.println("Elige una opcion de 0-6.");
                System.out.println("Opcio?:");
                String opcio = sc.nextLine();

                if (stringtoInt(opcio)) {
                    num = Integer.parseInt(opcio);
                    if (num <= 6 && num >= 0) {
                        opcioValida = true;
                    }
                }
            }

            switch (num) {
                case 0:
                    System.out.println("Adeu, fin del programa");
                    repetir = false;
                    break;
                case 1:
                    boolean valido = false;
                    while (!valido) {
                        System.out.println("Introdueix l'arxiu que vols llegir: ");
                        String nomXMl = sc.nextLine();
                        try {
                            assignaturas = Util.llegirXMlsecuencial(nomXMl);
                            for (Assignatura asignatura : assignaturas) {
                                asignatura.imprimir();
                            }
                            valido = true;
                        } catch (FileNotFoundException e) {
                            System.out.println("Error, no existeix l'arxiu " + nomXMl);
                        }
                    }
                    repetir = continuar();
                    break;
                case 2:
                    boolean valido2 = false;
                    while (!valido2) {
                        System.out.println("Introdueix l'arxiu que vols llegir: ");
                        String nomXMl = sc.nextLine();
                        try {
                            assignaturas = Util.llegirXMLsintactic(nomXMl);
                            for (Assignatura asignatura : assignaturas) {
                                asignatura.imprimir();
                            }
                            valido2 = true;
                        } catch (FileNotFoundException e) {
                            System.out.println("Error, no existeix l'arxiu " + nomXMl);
                        }
                    }
                    repetir = continuar();
                    break;
                case 3:
                    for (Assignatura assignatura : assignaturas) {
                        assignatura.imprimir();
                    }
                    repetir = continuar();
                    break;
                case 4:
                    assignaturas.add(Util.afegirAssignatura(assignaturas));
                    repetir = continuar();
                    break;
                case 5:
                    if (assignaturas.isEmpty()) {
                        System.out.println("Afegir una assignatura primer: ");
                        repetir = continuar();
                    } else {
                        Util.afegirAlumne(assignaturas);
                        repetir = continuar();
                    }
                    break;
                case 6:
                    Util.crearXML(assignaturas);
                    repetir = continuar();
                    break;

            }


        }


    }

    public static boolean continuar() {
        Scanner sc = new Scanner(System.in);
        boolean valido = false;
        boolean continuar = true;
        while (!valido) {
            System.out.print("\n--Continuar?:-- \n" +
                    "1. per a coninuar \n" +
                    "2. per a sortir\n");
            int opcion = sc.nextInt();

            if (opcion == 1) {
                continuar = true;
                valido = true;
            } else if (opcion == 2) {
                continuar = false;
                valido = true;
            } else {
                System.out.println("Escriu 1 o 2");

            }
        }
        return continuar;
    }

    public static boolean stringtoInt(String opcion) {
        try {
            Integer.parseInt(opcion);
            return true;
        } catch (NumberFormatException e) {
            return false;
        }
    }

    public static void imprimirMenu() {
        System.out.println(
                "0 - Sortir\n" +
                        "1 - Llegir d'un fitxer XML pel mètode seqüencial\n" +
                        /*"       Es demanarà el nom del fitxer\n" +*/
                        "2 - Llegir d'un fitxer XML pel mètode sintàctic\n" +
                        /*"       Es demanarà el nom del fitxer\n" +*/
                        "3 - Mostrar per pantalla totes les assignatures amb les seves dades (número, nom, durada i la llista d'alumnes)\n" +
                        "4 - Afegir una assignatura\n" +
                        /*"       Es demanaran les dades bàsiques (número, nom i durada)\n" +*/
                        "5 - Afegir un alumne a una assignatura\n" +
                        /*"       Es demanarà el número de l'assignatura (caldrà comprovar si existeix l'assignatura)\n" +
                        "       Es demanaran les de l'alumne (nom, dni i si és repetidor)\n" +
                        "       S'afegirà a la llista d'alumnes de l'assignatura\n" +*/
                        "6 - Guardar a disc en XML amb les assignatures\n");
    }
}
