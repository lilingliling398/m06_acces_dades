package Lilling;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Util {
    //OPCION 1 LLEER EL XML SECUENCIAL
    public static List<Assignatura> llegirXMlsecuencial(String nomXML) throws ParserConfigurationException, IOException, SAXException {
        Document doc = DocumentBuilderFactory.newInstance().newDocumentBuilder().parse(nomXML);
        Node arrel = doc.getDocumentElement();
        NodeList nodeListAssignatures = arrel.getChildNodes();
        List<Assignatura> assignaturas = new ArrayList<>();
        //recoroo raiz
        for (int i = 0; i < nodeListAssignatures.getLength(); i++) {

            if (nodeListAssignatures.item(i).getNodeName().equals("assignatura")) {
                NodeList listAsignatures = nodeListAssignatures.item(i).getChildNodes();
                Assignatura assignatura = new Assignatura();
                for (int j = 0; j < listAsignatures.getLength(); j++) {
                    switch (listAsignatures.item(j).getNodeName()) {
                        case "numero":
                            assignatura.setNumero(Integer.parseInt(listAsignatures.item(j).getTextContent()));
                            break;
                        case "nom":
                            assignatura.setNom(listAsignatures.item(j).getTextContent());
                            break;
                        case "durada":
                            assignatura.setDurada(Integer.parseInt(listAsignatures.item(j).getTextContent()));
                            break;
                        case "alumnes":
                            NodeList alumnes = listAsignatures.item(j).getChildNodes();

                            for (int k = 0; k < alumnes.getLength(); k++) {
                                if (alumnes.item(k).getNodeName().equals("alumne")) {
                                    NodeList alumne = alumnes.item(k).getChildNodes();
                                    Alumne alumneNou = new Alumne();
                                    for (int l = 0; l < alumne.getLength(); l++) {
                                        switch (alumne.item(l).getNodeName()) {
                                            case "nom":
                                                alumneNou.setNom(alumne.item(l).getTextContent());
                                                break;
                                            case "dni":
                                                alumneNou.setDni(alumne.item(l).getTextContent());
                                                break;
                                            case "repetidor":
                                                alumneNou.setRepetidor(Boolean.parseBoolean(alumne.item(l).getTextContent()));
                                                break;
                                        }
                                    }
                                    assignatura.alumnes.add(alumneNou);

                                }
                            }
                            break;
                    }
                }
                assignaturas.add(assignatura);
            }
        }
        return assignaturas;
    }

    public static List<Assignatura> llegirXMLsintactic(String nomXML) throws ParserConfigurationException, IOException, SAXException, XPathExpressionException {
        Document doc = DocumentBuilderFactory.newInstance().newDocumentBuilder().parse(nomXML);
        List<Assignatura> assignaturaList = new ArrayList<>();


        XPathExpression exprAssignatura = XPathFactory.newInstance().newXPath().compile("/assignatures/assignatura");
        NodeList llistaNodeAssignatura = (NodeList) exprAssignatura.evaluate(doc, XPathConstants.NODESET);


        for (int i = 0; i < llistaNodeAssignatura.getLength(); i++) {

            Assignatura nouAssignatura = new Assignatura();

            XPathExpression exprNum = XPathFactory.newInstance().newXPath().compile("numero");
            NodeList llistaNodesNum = (NodeList) exprNum.evaluate(llistaNodeAssignatura.item(i), XPathConstants.NODESET);
            nouAssignatura.setNumero(Integer.parseInt(llistaNodesNum.item(0).getTextContent()));

            XPathExpression exprNom = XPathFactory.newInstance().newXPath().compile("nom");
            NodeList llistaNodesNom = (NodeList) exprNom.evaluate(llistaNodeAssignatura.item(i), XPathConstants.NODESET);
            nouAssignatura.setNom(llistaNodesNom.item(0).getTextContent());

            XPathExpression exprDurada = XPathFactory.newInstance().newXPath().compile("durada");
            NodeList llistaNodesDurada = (NodeList) exprDurada.evaluate(llistaNodeAssignatura.item(i), XPathConstants.NODESET);
            nouAssignatura.setDurada(Integer.parseInt(llistaNodesDurada.item(0).getTextContent()));

            XPathExpression exprAlumne = XPathFactory.newInstance().newXPath().compile("alumnes/alumne");
            NodeList llistaNodesAlumne = (NodeList) exprAlumne.evaluate(llistaNodeAssignatura.item(i), XPathConstants.NODESET);

            for (int j = 0; j < llistaNodesAlumne.getLength(); j++) {
                Alumne alumne = new Alumne();

                XPathExpression exprANom = XPathFactory.newInstance().newXPath().compile("nom");
                NodeList llistaNodesANom = (NodeList) exprANom.evaluate(llistaNodesAlumne.item(j), XPathConstants.NODESET);
                alumne.setNom(llistaNodesANom.item(0).getTextContent());

                XPathExpression exprAdni = XPathFactory.newInstance().newXPath().compile("dni");
                NodeList llistaNodesAdni = (NodeList) exprAdni.evaluate(llistaNodesAlumne.item(j), XPathConstants.NODESET);
                alumne.setDni(llistaNodesAdni.item(0).getTextContent());

                XPathExpression exprArep = XPathFactory.newInstance().newXPath().compile("repetidor");
                NodeList llistaNodesArep = (NodeList) exprArep.evaluate(llistaNodesAlumne.item(j), XPathConstants.NODESET);
                alumne.setRepetidor(Boolean.parseBoolean(llistaNodesArep.item(0).getTextContent()));

                nouAssignatura.alumnes.add(alumne);
            }


            assignaturaList.add(nouAssignatura);
        }


        return assignaturaList;
    }

    //opcion cuatro
    public static Assignatura afegirAssignatura(List<Assignatura> assignaturas) {
        Scanner sc = new Scanner(System.in);
        Assignatura assignatura = new Assignatura();
        boolean valido = false;

        //sirve para no añadir una assignatura o numero repetido
        while (!valido) {
            System.out.println("\n--Afegir assignatura---\n" +
                    "Numero: ");
            int numero = sc.nextInt();

            if (assignaturas.isEmpty()) {
                assignatura.setNumero(numero);
                valido = true;
            } else {
                //repetido = false, si nunca encuentra un numero repetido
                boolean repetido = false;
                for (Assignatura assig : assignaturas) {
                    if (assig.getNumero() == numero) {
                        repetido = true;
                        break;
                    }
                }
                if (!repetido) {
                    assignatura.setNumero(numero);
                    valido = true;
                } else {
                    System.out.println("\nEl numero de l'assignatura ya existeix introduïu un altre ");
                }

            }


        }
        System.out.println("Nom : ");
        sc.nextLine();
        String nom = sc.nextLine();

        System.out.println("Durada: ");
        int durada = sc.nextInt();

        assignatura.setNom(nom);
        assignatura.setDurada(durada);
        return assignatura;
    }

    public static void afegirAlumne(List<Assignatura> assignaturas) {
        Scanner sc = new Scanner(System.in);
        Assignatura assignaturaEscollida = new Assignatura();

        System.out.println("Tria una assigntatura: ");
        for (Assignatura assignatura : assignaturas) {
            System.out.println(assignatura.getNumero() + ". " + assignatura.getNom());
        }

        int numAssig = 0;
        //parara de repetir cuando la asssignatura elegido es valido
        boolean repetir = true;
        while (repetir) {
            System.out.println("Opcio?: ");
            numAssig = sc.nextInt();
            for (Assignatura assignatura : assignaturas) {
                if (numAssig == assignatura.getNumero()) {
                    assignaturaEscollida = assignatura;
                    repetir = false;
                    break;
                }
            }
            if (repetir) {
                System.out.println("Tria un numero d'assigntatura existent");
            }
        }

        System.out.println("\nAfegir alumne------\n" +
                "Nom: ");
        sc.nextLine();
        String nom = sc.nextLine();

        System.out.println("DNI: ");
        String dni = sc.nextLine();

        System.out.println("Repetidor: ");
        boolean repetidor = sc.nextBoolean();

        assignaturaEscollida.getAlumnes().add(new Alumne(nom, dni, repetidor));
    }


    public static void crearXML(List<Assignatura> assignaturaList) throws ParserConfigurationException, TransformerException {
        Document document = DocumentBuilderFactory.newInstance().newDocumentBuilder().newDocument();

        Element assignatures = document.createElement("assignatures");
        document.appendChild(assignatures);

        for (Assignatura assignaturas : assignaturaList) {
            Element assignatura = document.createElement("assignatura");
            assignatures.appendChild(assignatura);

            Element numero = document.createElement("numero");
            numero.setTextContent(String.valueOf(assignaturas.getNumero()));
            assignatura.appendChild(numero);

            Element nom = document.createElement("nom");
            nom.setTextContent(assignaturas.getNom());
            assignatura.appendChild(nom);

            Element durada = document.createElement("durada");
            durada.setTextContent(String.valueOf(assignaturas.getDurada()));
            assignatura.appendChild(durada);

            Element alumnes = document.createElement("alumnes");
            assignatura.appendChild(alumnes);

            for (Alumne alumne : assignaturas.getAlumnes()) {
                afegirAlumnes(document, alumnes, alumne);
            }
        }

        Transformer trans = TransformerFactory.newInstance().newTransformer();
        StreamResult result = new StreamResult(new File("assignatures.xml"));
        DOMSource source = new DOMSource(document);
        trans.transform(source, result);

    }

    private static void afegirAlumnes(Document document, Element alumnes, Alumne alumne) {
        Element alumneNOU = document.createElement("alumne");
        alumnes.appendChild(alumneNOU);

        Element nom = document.createElement("nom");
        nom.setTextContent(alumne.getNom());
        alumneNOU.appendChild(nom);

        Element dni = document.createElement("dni");
        dni.setTextContent(alumne.getDni());
        alumneNOU.appendChild(dni);

        Element repetidor = document.createElement("repetidor");
        repetidor.setTextContent(String.valueOf(alumne.isRepetidor()));
        alumneNOU.appendChild(repetidor);
    }
}








