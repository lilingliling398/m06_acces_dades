package Lilling;

import java.util.ArrayList;
import java.util.List;

public class Assignatura {
    private int numero;
    private String nom;
    private int durada;
    List<Alumne> alumnes = new ArrayList<>();


    public Assignatura() {

    }

    public Assignatura(int numero, String nom, int durada, List<Alumne> alumnes) {
        this.numero = numero;
        this.nom = nom;
        this.durada = durada;
        this.alumnes = alumnes;
    }

    public void imprimir(){
        System.out.println("===DADES DE L'ASSIGNATURA===");
        System.out.println("Numero: "+this.numero);
        System.out.println("Nom: "+this.nom);
        System.out.println("Durada: "+this.durada);
        for (Alumne alumne : this.alumnes) {
            alumne.imprimir();
        }
        System.out.println("============================");
    }

    public int getNumero() {
        return numero;
    }

    public void setNumero(int numero) {
        this.numero = numero;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public int getDurada() {
        return durada;
    }

    public void setDurada(int durada) {
        this.durada = durada;
    }

    public List<Alumne> getAlumnes() {
        return alumnes;
    }

    public void setAlumnes(List<Alumne> alumnes) {
        this.alumnes = alumnes;
    }
}
