package Util;

import java.util.Scanner;

public class Leer {
    static Scanner sc = new Scanner(System.in);

    public static int stringToInt() {
        String valor = sc.nextLine();
        int resultat = 0;
        try {
            resultat = Integer.parseInt(valor);
        } catch (NumberFormatException e) {
            System.out.print("Introdueix un int: ");
            resultat = stringToInt();
        }
        return resultat;
    }

    public static int opcionInt(int min, int max) {
        int opcion = stringToInt();
        if (opcion >= min && opcion <= max) {
            return opcion;
        } else {
            System.out.print("Inrodueix un int entre " + min + "-" + max + ": ");
            opcion = opcionInt(min, max);
        }
        return opcion;
    }


}

    /*boolean acabarMenu = false;
        while (!acabarMenu) {


                switch () {
                case 1:

                break;
                case 2:

                break;
                case 3:

                break;
                case 4:

                break;
                case 5:

                break;
                case 0:
                acabarMenu = true;
                break;
                }
                }*/
