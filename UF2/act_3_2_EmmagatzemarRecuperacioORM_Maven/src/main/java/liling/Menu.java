package liling;

import Util.Leer;

public class Menu {
    public static void main(String[] args) {

        boolean acabarMenu = false;
        while (!acabarMenu) {

            imprimirMenu();

            System.out.print("Opcio?: ");
            int opcion = Leer.opcionInt(0, 7);
            System.out.println();

            switch (opcion) {
                case 1:
                    UtilORM.eliminarClient();
                    break;
                case 2:
                    UtilORM.actualitzarClient();
                    break;
                case 3:
                    UtilORM.cercaClient();
                    break;
                case 4:
                    UtilORM.altaClient();
                    break;
                case 5:
                    UtilORM.altaComanda();
                    break;
                case 6:
                    UtilORM.mostarComandes();
                    break;
                case 7:
                    UtilORM.crearResum();
                    break;
                case 0:

                    acabarMenu = true;
                    break;
            }
        }
    }

    private static void imprimirMenu() {
        System.out.println("");
        System.out.println("__MENU__________________________________________");
        System.out.println("1. Eliminació d'un client");
        System.out.println("2. Actualitzar dades d'un client");
        System.out.println("3. Cerca de nom");
        System.out.println("4. Alta d'un nou client");
        System.out.println("5. Alta d'una nova comanda");
        System.out.println("6. Mostrar per pantalla les comandes d'un client");
        System.out.println("7. Generació de resum de facturació");

    }
}
