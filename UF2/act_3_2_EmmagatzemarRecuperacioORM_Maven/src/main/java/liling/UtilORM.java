package liling;

import Util.Leer;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.hibernate.query.Query;

import java.sql.Date;
import java.util.*;

public class UtilORM {

    //1.- Eliminació d'un client.
    public static void eliminarClient() {
        SessionFactory factory = new Configuration().configure("hibernate.cfg.xml").addAnnotatedClass(Client.class).buildSessionFactory();
        Session session = factory.openSession();

        try {
            Client clietSelect = seleccionarClient(session);

            session.beginTransaction();
            List<Comanda> comandaList = session.createQuery("from Comanda C where C.dni_client= " + clietSelect.getDni()).list();
            for (Comanda comanda : comandaList) {
                session.delete(comanda);
            }
            session.delete(clietSelect);
            session.getTransaction().commit();
            System.out.println("Client esborrat correctament");
        } finally {
            session.close();
            factory.close();
        }

    }

    //2.- Actualització de les dades d'un client.
    public static void actualitzarClient() {
        Scanner sc = new Scanner(System.in);
        SessionFactory factory = new Configuration().configure("hibernate.cfg.xml").addAnnotatedClass(Client.class).buildSessionFactory();
        Session session = factory.openSession();

        Client clientSelect = seleccionarClient(session);
        System.out.println("==DADES NOU DEL DNI: " + clientSelect.getDni() + "===");

        System.out.print("Nom: ");
        String nom = sc.nextLine();

        System.out.print("Es premiun (s/n): ");
        boolean premiun = Leer.pedirBoolean();

        try {
            session.beginTransaction();
            //manera de Update
            //Copger el cliente de la BBDDD y cambiar los dados con un set
            clientSelect.setNom(nom);
            clientSelect.setPremium(premiun);
            session.getTransaction().commit();

            System.out.println("Client actualitzat correctamnet");
        } finally {
            session.close();
            factory.close();
        }

    }

    private static Client seleccionarClient(Session session) {
        List<Client> listClient = session.createQuery("from Client").getResultList();
        if (listClient.isEmpty()) {
            System.out.println("No hi ha clients per seleccionar.");
            return null;
        } else {
            for (Client c : listClient) {
                System.out.printf("%d) %d %s\n", listClient.indexOf(c) + 1, c.getDni(), c.getNom());
            }

            System.out.print("Selecciona un client: ");
            int index = Leer.opcionInt(1, listClient.size());

            Client client = listClient.get(index - 1);

            return session.get(Client.class, client.getDni());
        }
    }


    //3.- Cerca per nom de client
    public static void cercaClient() {
        Scanner sc = new Scanner(System.in);

        SessionFactory factory = new Configuration().configure("hibernate.cfg.xml").addAnnotatedClass(Client.class).buildSessionFactory();
        Session session = factory.openSession();

        System.out.println("\n***CERCADOR DE NOM: ***");
        System.out.print("Cerca: ");
        String palabra = sc.nextLine();

        try {
            String setencia = "select C.nom from Client C where C.nom like '%" + palabra + "%'";
            Query query = session.createQuery(setencia);
            List listaNom = query.list();

            System.out.println("Resultat de la cerca: ");
            for (Object nom : listaNom) {
                System.out.println(nom.toString());
            }
        } finally {
            session.close();
            factory.close();
        }


    }

    //4.- Alta d'un nou client
    public static void altaClient() {
        Client clientNou = new Client(true);

        SessionFactory factory = new Configuration().configure("hibernate.cfg.xml").addAnnotatedClass(Client.class).buildSessionFactory();
        Session session = factory.openSession();

        String setencia = "select C.dni from Client C";
        Query query = session.createQuery(setencia);
        List listaDni = query.list();

        try {
            if (listaDni.contains(clientNou.getDni())) {
                System.out.println("El client amb ja existeix en la BBBDD Dni:" + clientNou.getDni());
            } else {
                session.beginTransaction();
                session.save(clientNou);
                session.getTransaction().commit();
                System.out.println("Client guardat correctamnet");
            }
        } finally {
            session.close();
            factory.close();
        }

    }

    //5.- Alta d'una nova comanda
    public static void altaComanda() {
        SessionFactory factory = new Configuration().configure("hibernate.cfg.xml").addAnnotatedClass(Client.class).buildSessionFactory();
        Session session = factory.openSession();
        session.beginTransaction();
        Client clientSelect;
        if ((clientSelect = seleccionarClient(session)) == null) {
            System.out.println("No hi ha clients per seleccionar");
        } else {
            System.out.println("DADES DEL LA COMANDA: ");
            System.out.print("Num de la comanda: ");
            int numComanda = Leer.pedirInt();

            List<Integer> listaNumComanda = session.createQuery("select C.num_comanda from Comanda C").list();

            try {
                if (listaNumComanda.contains(numComanda)) {
                    System.out.println("La comanda ja existeix en la BBBDD");
                } else {
                    System.out.print("Preu total: ");
                    double preuTotal = Leer.pedirDouble();

                    System.out.println("Data(dd/mm/aaaa)");
                    Date data = Leer.pedirData();

                    int dniClient = clientSelect.getDni();
                    Comanda comanda = new Comanda(numComanda, preuTotal, data, dniClient);


                    session.save(comanda);
                    session.getTransaction().commit();
                    System.out.println("Comanda guardada correctament");
                }
            } finally {
                session.close();
                factory.close();
            }
        }
    }

    //6.- Mostrar per pantalla les comandes d'un client
    public static void mostarComandes() {
        SessionFactory factory = new Configuration().configure("hibernate.cfg.xml").addAnnotatedClass(Client.class).buildSessionFactory();
        Session session = factory.openSession();
        Client clientSelect;
        if ((clientSelect = seleccionarClient(session)) == null) {
            System.out.println("No hi ha clients per seleccionar");
        } else {
            System.out.println("Clients del client " + clientSelect.getNom());
            List<Comanda> comandaList = session.createQuery("from Comanda C where C.dni_client=" + clientSelect.getDni()).list();
            for (Comanda comanda : comandaList) {
                comanda.imprimir();
            }
        }
    }

    //7. Mostrar per pantalla un resum de tots els clients amb la quantitat total que han facturat, ordenat de més facturació a menys facturació.
    public static void crearResum() {
          SessionFactory factory = new Configuration().configure("hibernate.cfg.xml").addAnnotatedClass(Client.class).buildSessionFactory();
        Session session = factory.openSession();
        SortedMap<Double, String> listaOrdenat = new TreeMap<>(new Comparator<Double>() {
            @Override
            public int compare(Double o1, Double o2) {
                return o2.compareTo(o1);
            }
        });

        List<Client> clientList = session.createQuery("from Client").list();
        for (Client client : clientList) {

            double preuTotal = 0.;
            List<Comanda> comandaList = session.createQuery("from Comanda C where C.dni_client=" + client.getDni()).list();

            if (!comandaList.isEmpty()) {
                for (Comanda comanda : comandaList) {
                    preuTotal += comanda.getPreu_total();
                }

                listaOrdenat.put(preuTotal, client.getNom());
            }
        }

        for (Map.Entry<Double, String> mapEntry : listaOrdenat.entrySet()) {
            System.out.println(mapEntry.getValue() + ": " + mapEntry.getKey() + "€");

        }
    }
}
