package liling;

import Util.Leer;

import javax.persistence.*;
import java.util.Scanner;

@Entity
@Table(name = "client")
public class Client {
    @Id
    @Column(name = "Dni")
    private int dni;

    @Column(name = "Nom")
    private String nom;

    @Column(name = "Premium")
    private boolean premium;


    public Client(int dni, String nom, boolean premiun) {
        this.dni = dni;
        this.nom = nom;
        this.premium = premiun;

    }
    
    public Client(){}

    public Client(boolean alta) {
        if (alta) {
            Scanner sc = new Scanner(System.in);

            System.out.println("\n***DADES DEL NOU CLIENT: ****");

            System.out.print("DNI: ");
            dni = Leer.pedirInt();

            System.out.print("Nom: ");
            nom = sc.nextLine();

            System.out.print("premium (s/n): ");
            premium = Leer.pedirBoolean();
        }
    }

    public void imprimir() {
        System.out.println("\n====Dades del client===============");
        System.out.println("Nom: " + nom);
        System.out.println("DNI: " + dni);
        System.out.println("Es premiun: " + premium);

    }

    public int getDni() {
        return dni;
    }

    public void setDni(int dni) {
        this.dni = dni;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public boolean isPremium() {
        return premium;
    }

    public void setPremium(boolean premium) {
        this.premium = premium;
    }



}
