DROP TABLE IF EXISTS RESUM_FACTURACIO;

CREATE TABLE RESUM_FACTURACIO (
	Mes INT(2),
	Any INT(4),
	DniClient INT(8),
	Quantitat FLOAT(10, 2),
    CONSTRAINT pk_facturacion PRIMARY KEY (Mes, any, DniClient),    
	FOREIGN KEY (DniClient) REFERENCES client(Dni) 
);

DROP PROCEDURE IF EXISTS crea_resum_facturacio;
DELIMITER //
CREATE PROCEDURE crea_resum_facturacio(p_mes INT(2), p_any INT(4)) 
BEGIN
DECLARE v_dniClient INT(8);
DECLARE v_precio double(10,2);
DECLARE v_quantitat double(10,2);
DECLARE acaba INT DEFAULT FALSE; 


DECLARE c_clientes CURSOR FOR select DISTINCT DniClient from comanda where MONTH(Data)=p_mes AND YEAR(Data)=p_any;
DECLARE c_precios CURSOR FOR select PreuTotal from comanda WHERE DniClient=v_DniClient AND MONTH(Data)=p_mes AND YEAR(Data)=p_any;
DECLARE CONTINUE HANDLER FOR NOT FOUND SET acaba = TRUE; 

-- lista clientes
OPEN c_clientes; 
	read_loop: LOOP
		FETCH c_clientes INTO v_dniClient;
		IF acaba THEN
			LEAVE read_loop;
		END IF;
        -- lista precios
        SET v_quantitat=0;
		OPEN c_precios; 
			read_loop2: LOOP
			FETCH c_precios into v_precio;
			IF acaba THEN
				LEAVE read_loop2;
			END IF;
			SET v_quantitat=(v_quantitat+v_precio);
		END LOOP;
        SET acaba = FAlSE;
		CLOSE c_precios;
     insert into resum_facturacio values (p_mes, p_any, v_dniClient, v_quantitat);
	END LOOP;
CLOSE c_clientes;   
END; //

