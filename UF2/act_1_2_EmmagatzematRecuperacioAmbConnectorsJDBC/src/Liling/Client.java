package Liling;

import java.util.ArrayList;
import java.util.List;

public class Client {
    private int dni;
    private String nom;
    private boolean premium;
    private List<Comanda> comandas;

    public Client(int dni, String nom, boolean premiun, List<Comanda> comandas) {
        this.dni = dni;
        this.nom = nom;
        this.premium = premiun;
        this.comandas = comandas;
    }
    public Client(int dni, String nom, boolean premiun) {
        this.dni = dni;
        this.nom = nom;
        this.premium = premiun;
        comandas = new ArrayList<>();
    }

    public Client(){
        comandas = new ArrayList<>();
    }

    public void imprimir() {
        System.out.println("\n====Dades del client===============");
        System.out.println("Nom: "+nom);
        System.out.println("DNI: "+dni);
        System.out.println("Es premiun: "+ premium);
        for (Comanda comanda : this.comandas) {
            comanda.imprimir();
        }
    }

    public int getDni() {
        return dni;
    }

    public void setDni(int dni) {
        this.dni = dni;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public boolean isPremium() {
        return premium;
    }

    public void setPremium(boolean premium) {
        this.premium = premium;
    }

    public List<Comanda> getComandas() {
        return comandas;
    }

    public void setComandas(List<Comanda> comandas) {
        this.comandas = comandas;
    }
}
