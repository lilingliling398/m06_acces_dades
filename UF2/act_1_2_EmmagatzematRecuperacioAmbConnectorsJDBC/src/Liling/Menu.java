package Liling;

import Util.Leer;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class Menu {
    public static void main(String[] args) throws SQLException {
        List<Client> clientList = new ArrayList<>();
        UtilBBDD utilBBDD = new UtilBBDD();

        if (utilBBDD.getCon() != null) {
            //1.3
            clientList = utilBBDD.llegirBBDD();


            boolean acabarMenu = false;
            while (!acabarMenu) {

                imprimirMenu();

                System.out.print("Opcio?: ");
                int opcion = Leer.opcionInt(0, 7);
                System.out.println();

                switch (opcion) {
                    case 1:
                        //utilBBDD.crearTaula();
                        utilBBDD.eliminarClient(clientList);

                        break;
                    case 2:
                        //clientList = utilBBDD.llegirBBDD();
                        utilBBDD.actualizarDades(clientList);
                        break;
                    case 3:
                        //utilBBDD.insertarBBDD(clientList);
                        utilBBDD.buscarClient(clientList);
                        break;
                    case 4:
                        utilBBDD.altaClient(clientList);

                        break;
                    case 5:
                        if (clientList.isEmpty()) {
                            System.out.println("No hi ha clients");
                        } else {
                            utilBBDD.altaComanda(clientList);
                        }
                        break;
                    case 6:
                        if (clientList.isEmpty()) {
                            System.out.println("No hi ha clients per seleccionar");
                        } else {
                            utilBBDD.mostrarClient(clientList);
                        }
                        break;
                    case 7:
                        utilBBDD.crearFactura();
                        break;
                    case 0:
                        utilBBDD.cerrarConecxio();
                        acabarMenu = true;
                        break;
                }
            }
        }
    }

    private static void imprimirMenu() {
        System.out.println("");
        System.out.println("__MENU__________________________________________");
        System.out.println("1. Eliminació d'un client");
        System.out.println("2. Actualitzar dades d'un client");
        System.out.println("3. Cerca de nom");
        System.out.println("4. Alta d'un nou client");
        System.out.println("5. Alta d'una nova comanda");
        System.out.println("6. Mostrar per pantalla les comandes d'un client");
        System.out.println("7. Generació de resum de facturació");

    }
}
