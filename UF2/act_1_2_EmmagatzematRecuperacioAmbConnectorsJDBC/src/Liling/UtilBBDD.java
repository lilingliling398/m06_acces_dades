package Liling;

import Util.Leer;

import java.sql.*;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class UtilBBDD {
    static Scanner sc = new Scanner(System.in);
    private Connection con;

    public UtilBBDD() {
        String url = "jdbc:mysql://localhost/lamevabbdd";
        String usuari = "liling";
        String password = "jupiter";
        try {
            con = DriverManager.getConnection(url, usuari, password);
        } catch (SQLException throwables) {
            System.out.println("ERROR en la conexió amb la BBDD");
        }
    }
    public void cerrarConecxio(){
        try {
            con.close();
        } catch (SQLException e) {
            System.out.println("ERROR, no s'ha pogut tancar la conecxió");
        }
    }

    public Connection getCon() {
        return con;
    }

    /*dni és sense la lletra i identifica el client
    nom és un text
    premium és un booleà*/
    //1
    public void crearTaula() {
        try {
            String sentencia =
                    "CREATE TABLE Client (" +
                            "Dni int(8) NOT NULL," +
                            "Nom varchar(30)," +
                            "Premium boolean," +
                            "PRIMARY KEY (Dni)" +
                            ")";
            Statement statement = con.createStatement();
            statement.execute(sentencia);

            System.out.println("Taula Client creada correctament");

        } catch (SQLException e) {
            System.out.println("ERROR al crear la taula Client, pot ser que ya existeix");
        }

        try {
            String sentencia =
                    "CREATE TABLE Comanda (" +
                            "NumComanda int NOT NULL," +
                            "PreuTotal double(10, 2) NOT NULL," +
                            "Data DATE," +
                            "DniClient INT(8) NOT NULL," +
                            "PRIMARY KEY (NumComanda)," +
                            "FOREIGN KEY (DniClient) REFERENCES Client(Dni)" +
                            ")";
            Statement statement = con.createStatement();
            statement.execute(sentencia);
            System.out.println("Taula Comanda creada correctament");
        } catch (SQLException e) {
            System.out.println("ERROR al crear la taula Comanda, pot ser que ya existeix");
        }
    }

    //2
    public List<Client> llegirBBDD() {
        List<Client> clientList = new ArrayList<>();
        int contador = 0;
        try {
            Statement statement = con.createStatement();
            Statement statement1 = con.createStatement();
            String sentencia = "select * from Client;";
            ResultSet rs = statement.executeQuery(sentencia);

            while (rs.next()) {
                Client client = new Client();
                client.setDni(rs.getInt("Dni"));
                client.setNom(rs.getString("Nom"));
                client.setPremium(rs.getBoolean("Premium"));


                String sentenciaComando = "SELECT * from Comanda WHERE DniClient=" + client.getDni() + ";";
                ResultSet resultSet = statement1.executeQuery(sentenciaComando);
                while (resultSet.next()) {
                    Comanda comanda = new Comanda();
                    comanda.setNum_comanda(resultSet.getInt("NumComanda"));
                    comanda.setPreu_total(resultSet.getDouble("PreuTotal"));
                    comanda.setData(resultSet.getDate("Data").toLocalDate());
                    comanda.setDni_client(resultSet.getInt("DniClient"));
                    client.getComandas().add(comanda);
                }
                clientList.add(client);
                contador++;
            }
        } catch (SQLException e) {
            System.out.println("ERROR, no s'ha pogut llegir la BBDD");
            System.out.println("S'ha llegit " + contador + " clients");
        }
        return clientList;
    }

    //3
    public void insertarBBDD(List<Client> clientList) {
        try {
            Statement statement = con.createStatement();

            String setenciaDelete = "delete from comanda";
            statement.execute(setenciaDelete);
            String setenciaDelete1 = "delete from client";
            statement.execute(setenciaDelete1);

            for (Client client : clientList) {
                String sentencia = "INSERT INTO client(Dni, Nom, Premium) values (?,?,?);";
                PreparedStatement preparedStatement = con.prepareStatement(sentencia);
                preparedStatement.setInt(1, client.getDni());
                preparedStatement.setString(2, client.getNom());
                preparedStatement.setBoolean(3, client.isPremium());
                preparedStatement.executeUpdate();
                try {
                    for (Comanda comanda : client.getComandas()) {
                        String sentenciaComanda = "INSERT INTO comanda(NumComanda, PreuTotal, Data, DniClient ) values (?,?,?,?);";
                        PreparedStatement preparedStatement2 = con.prepareStatement(sentenciaComanda);
                        preparedStatement2.setInt(1, comanda.getNum_comanda());
                        preparedStatement2.setDouble(2, comanda.getPreu_total());
                        preparedStatement2.setDate(3, Date.valueOf(comanda.getData()));
                        preparedStatement2.setInt(4, comanda.getDni_client());
                        preparedStatement2.executeUpdate();
                    }
                } catch (SQLException e) {
                    System.out.println("ERROR, no s'ha pogut crear la comanda al BBDD");
                }
            }
        } catch (SQLException e) {
            System.out.println("ERROR, no s'ha pogut crear el client al BBDD");
        }
    }

    // eliminació d'un client. Se selecciona el client i s'elimina el client i totes les seves comandes de la BBDD
    //1.3-1
    public void eliminarClient(List<Client> clientList) {
        int client = opcioClient(clientList);
        int dni = clientList.get(client).getDni();

        try {
            Statement statement = con.createStatement();

            String setenciaDelete = "delete from comanda where dniClient=" + dni + ";";
            statement.execute(setenciaDelete);
            String setenciaDelete1 = "delete from client where dni=" + dni + ";";
            statement.execute(setenciaDelete1);
        } catch (SQLException e) {
            System.out.println("ERROR, no s'ha pogut borrar el client a la DDBB");
            //si falla se deshace las acciones!!!!!!!!!
        }
    }

    //actualització de les dades d'un client. Se selecciona un client,
    // i es tornen a introduir les seves dades (les que no siguin PK, és a dir, nom i premium).
    // S'actualitza la informació a la BBDD
    //1.3-2
    public void actualizarDades(List<Client> clientList) {
        int client = opcioClient(clientList);
        int dni = clientList.get(client).getDni();
        String nom;
        boolean premium;
        System.out.println("\n***INTRODUÏR DADES NOU CLIENT: ****");

        System.out.print("Nom: ");
        nom = sc.nextLine();

        System.out.print("premium: ");
        premium = sc.nextBoolean();
        sc.nextLine();

        try {
            String setenActualizar = "UPDATE client SET Nom=?, Premium=? WHERE dni=?;";
            PreparedStatement ps = con.prepareStatement(setenActualizar);
            ps.setString(1, nom);
            ps.setBoolean(2, premium);
            ps.setInt(3, dni);
            ps.executeUpdate();

            clientList.get(client).setNom(nom);
            clientList.get(client).setPremium(premium);
        } catch (SQLException e) {
            System.out.println("No s'ha pogut actualitzar les dades del client: " + dni);
        }
    }

    //mostrar per pantalla els clients de la BBDD que el seu nom comenci pel text introduït per teclat.
    //Per exemple, si l'usuari introdueix "Jo", es mostrarien les Joanes, els Joseps, els Jordis...
    //1.3-3
    public void buscarClient(List<Client> clientList) {
        String palabra;
        int x;
        System.out.println("\n***CERCADOR DE NOM: ***");
        System.out.print("Cerca: ");
        palabra = sc.nextLine();

        try {
            Statement statement = con.createStatement();
            String setencia = "select nom from client where nom LIKE '" + palabra + "%';";
            ResultSet resultado = statement.executeQuery(setencia);

            while (resultado.next()) {
                System.out.println(resultado.getString("Nom"));
            }
        } catch (SQLException e) {
            System.out.println("ERROR, no s'ha pogut fer la cerca");
        }
    }

    //Demana les dades del client (dni, nom i si és "premium" o no) i l'afegeix a la llista de clients.
    // Aquesta operació no emmagatzema res a la BBDD, ja que l'emmagatzemat es fa a l'opció 3.
    //4
    public void altaClient(List<Client> clientList) {
        int dni;
        String nom = null;
        boolean premium = false;

        try {
            System.out.println("\n***DADES DEL NOU CLIENT: ****");

            System.out.print("DNI: ");
            dni = Leer.pedirInt();

            if (existeDniArray(clientList, dni)) {
                System.out.println("Aquest DNI ya existeix en la llista client");
            } else {
                System.out.print("Nom: ");
                nom = sc.nextLine();

                System.out.print("premium: ");
                premium = sc.nextBoolean();
                sc.nextLine();

                clientList.add(new Client(dni, nom, premium));
            }
            String sentencia = "INSERT INTO client(Dni, Nom, Premium) values (?,?,?);";
            PreparedStatement preparedStatement = con.prepareStatement(sentencia);
            preparedStatement.setInt(1, dni);
            preparedStatement.setString(2, nom);
            preparedStatement.setBoolean(3, premium);
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            System.out.println("ERROR, no s'ha pogut crear el client a la DDBB");
            //si falla se deshace las acciones!!!!!!!!!
        }

    }

    //Demana en primer lloc les dades del client que està relacionat amb la comanda.
    // Per fer-ho, mostra un menú numerat amb els DNI i noms dels clients.
    // L'usuari selecciona el nom del client relacionat, introduint el número. Després es demanen les dades bàsiques de la comanda (número, preu i data).
    //5
    public void altaComanda(List<Client> clientList) {
        int numero;
        double preu = 0;
        LocalDate data = null;
        int dni = 0;

        int client = opcioClient(clientList);

        System.out.println("\n***DADES DEL COMANDA: ***");
        System.out.print("Numero: ");
        numero = Leer.pedirInt();

        if (existeComandoArray(clientList, numero)) {
            System.out.println("Aquest comanda ya existeix en la llista");
        } else {
            System.out.print("Preu: ");
            preu = Leer.pedirDouble();

            System.out.print("Data (dd/MM/yyyy): ");
            data = Leer.pedirLocalData();

            dni = clientList.get(client).getDni();

            clientList.get(client).getComandas().add(new Comanda(numero, preu, data, dni));
        }
        //1.3
        try {
            String sentenciaComanda = "INSERT INTO comanda(NumComanda, PreuTotal, Data, DniClient ) values (?,?,?,?);";
            PreparedStatement preparedStatement2 = con.prepareStatement(sentenciaComanda);
            preparedStatement2.setInt(1, numero);
            preparedStatement2.setDouble(2, preu);
            preparedStatement2.setDate(3, Date.valueOf(data));
            preparedStatement2.setInt(4, dni);
            preparedStatement2.executeUpdate();
        } catch (SQLException e) {
            System.out.println("ERROR, no s'ha pogut crear el client a la DDBB");
            //si falla se deshace las acciones!!!!!!!!!
        }

    }

    //Es demanarà en primer lloc el client, de la mateixa manera que a l'opció 5. Un cop seleccionat el client, es mostraran per pantalla totes les dades de la comanda.
    //6
    public void mostrarClient(List<Client> clientList) {
        int clientSelec = opcioClient(clientList);

        clientList.get(clientSelec).imprimir();
    }

    //Cal comprovar que el mes i l'any siguin vàlids i, en cas que no ho siguin, mostrar un error a l'usuari i no cridar al procediment.
    //Si en algún punt de l'execució del procediment es produeix un error,
    // cal desfer tots els canvis realitzats, és a dir, totes les insercions a la taula resum_facturacio.
    // Es mostrarà també un error a l'usuari. Recorda que, per fer-ho, caldrà que estableixis inicialment el setAutoCommit de la connexió a fals.
    //7
    public void crearFactura(){
        String url = "jdbc:mysql://localhost/lamevabbdd";
        String usuari = "liling";
        String password = "jupiter";
        Connection con2 = null;
        try {
            con2 = DriverManager.getConnection(url, usuari, password);
        } catch (SQLException throwables) {
            System.out.println("ERROR en la conexió amb la BBDD");
        }

        int mes;
        int any;
        PreparedStatement ps = null;
        System.out.println("\n***GENERAR RESUM FACTURA: ***");
        System.out.print("Mes: ");
        mes = Leer.pedirInt();
        System.out.print("Any: ");
        any = Leer.pedirInt();


        try {
            assert con2 != null;
            con2.setAutoCommit(false);
            Statement statement = con2.createStatement();
            String isValidMesAny = "select DISTINCT DniClient from comanda where MONTH(Data)=" + mes + " AND YEAR(Data)=" + any + ";";
            ResultSet resultSet = statement.executeQuery(isValidMesAny);
            //PARA COMPROBAR QUE EL MES Y AÑO SEAN VALIDOS EN LA BBDD
            if (resultSet.next()) {
                String borrar = " delete from resum_facturacio where mes=? and any=?;";
                ps = con2.prepareStatement(borrar);
                ps.setInt(1, mes);
                ps.setInt(2, any);
                ps.executeUpdate();

                String setencia= "call crea_resum_facturacio(?, ?);";
                ps = con2.prepareStatement(setencia);
                ps.setInt(1, mes);
                ps.setInt(2, any);
                ps.executeUpdate();
                //error de prueba, (cal desfer tots els canvis realitzats, és a dir, totes les insercions a la taula resum_facturacio)
                /*
                String error= "";
                ps = con2.prepareStatement(error);
                ps.executeUpdate();*/

                con2.commit();
                System.out.println("Resum creat correctament");
            }else {
                System.out.println("ERROR, mes i/o any no valids");
            }
        } catch (SQLException e) {
            System.out.println("ERROR, no s'ha pogut fer el resum del la factura");
        }
        try {
            con2.close();
        } catch (SQLException e) {
            System.out.println("ERROR, no s'ha pogut tancar correctament");
        }
    }

    public int opcioClient(List<Client> clientList) {
        System.out.println("\nSelecciona un client: ");

        for (Client client : clientList) {
            System.out.printf("%d) %d - %s\n", clientList.indexOf(client) + 1, client.getDni(), client.getNom());
        }
        System.out.print("Seleccioneu el client de la comanda: ");
        int cliente = Leer.opcionInt(1, clientList.size());
        return cliente - 1;
    }

    public boolean existeDniArray(List<Client> clientList, int dni) {
        boolean repetido = false;
        for (Client client : clientList) {
            if (client.getDni() == dni) {
                repetido = true;
                break;
            }
        }
        return repetido;
    }

    /*public boolean existeDniSQl(String sSQL, int dni) {
        try {
            PreparedStatement ps = con.prepareStatement(sSQL);
            ps.setString(1, dni);
            ResultSet rs = ps.executeQuery();
            return rs.next();

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }*/

    public boolean existeComandoArray(List<Client> clientList, int numComanda) {
        boolean repetido = false;
        for (Client client : clientList) {
            for (Comanda comanda : client.getComandas()) {
                if (comanda.getNum_comanda() == numComanda) {
                    repetido = true;
                    break;
                }
            }
        }
        return repetido;
    }

}
