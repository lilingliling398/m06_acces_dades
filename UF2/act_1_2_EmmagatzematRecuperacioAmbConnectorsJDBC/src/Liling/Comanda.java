package Liling;

import java.time.LocalDate;

public class Comanda {
    private int num_comanda;
    private double preu_total;
    private LocalDate data;
    private int dni_client;

    public Comanda(int num_comanda, double preu_total, LocalDate data, int dni_client) {
        this.num_comanda = num_comanda;
        this.preu_total = preu_total;
        this.data = data;
        this.dni_client = dni_client;
    }

    public Comanda() {
    }

    public void imprimir() {
        System.out.println("    ====Dades de la comanda========");
        System.out.println("    Numero de comanda: "+num_comanda);
        System.out.println("    Preu total: "+preu_total);
        System.out.println("    Data: "+data);
    }

    public int getNum_comanda() {
        return num_comanda;
    }

    public void setNum_comanda(int num_comanda) {
        this.num_comanda = num_comanda;
    }

    public double getPreu_total() {
        return preu_total;
    }

    public void setPreu_total(double preu_total) {
        this.preu_total = preu_total;
    }

    public LocalDate getData() {
        return data;
    }

    public void setData(LocalDate data) {
        this.data = data;
    }

    public int getDni_client() {
        return dni_client;
    }

    public void setDni_client(int dni_client) {
        this.dni_client = dni_client;
    }
}
