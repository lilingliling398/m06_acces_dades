package Liling;


import Varies.Leer;

import java.util.ArrayList;
import java.util.List;

public class Menu {
    public static void main(String[] args) {
        List<Politic> politics = new ArrayList<>();
        Politic nouPolitic = new Politic();

        boolean bucle;
        int opcion;

        boolean salirMenu = false;
        while (!salirMenu) {
            imprimirMenu();

            opcion = Leer.opcion(0, 3);

            switch (opcion) {
                case 1:
                    Politic_Util.crearTaula();
                    break;
                case 2:
                    nouPolitic = Politic_Util.pedirDades();
                    Politic_Util.insertarDadesBBDD(nouPolitic);
                    break;
                case 3:
                    politics = Politic_Util.llegirTaula();
                    for (Politic politic : politics) {
                        System.out.println("DADES DEL POLITIC");
                        System.out.println(politic.toString());
                    }
                    break;
                case 0:
                    salirMenu = true;
                    break;
            }

        }
    }

    public static void imprimirMenu() {
        System.out.println("\n==MENU==");
        System.out.println("0. Sortir");
        System.out.println("1. Crear taula");
        System.out.println("2. Crear nou politic");
        System.out.println("3. LListar tots els politics");

    }
}
