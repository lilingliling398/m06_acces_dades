package Liling;

import Varies.Leer;

import java.sql.*;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Politic_Util {
    static Scanner sc = new Scanner(System.in);
    //Class.forName("com.mysql.cj.jdbc.Driver");
    static String url = "jdbc:mysql://localhost/lamevabbdd";//jdbc:mysql://localhost/lamevabbdd";
    static String usuari = "liling";
    static String password = "jupiter";
    static Connection con = null;

    public static void crearTaula() {
        // opcional! comprobar si existe una tabbla concreta
        //DatabaseMetaData dbm = con.getMetaData(); // check if "employee" table is there ResultSet tables = dbm.getTables(null, null, "employee", null); if (tables.next()) { // Table exists } else { // Table does not exist }

        try {
            con = DriverManager.getConnection(url, usuari, password);
            try {
                String drop = "DROP TABLE POLITIC";

                String sentenciaSQL = "CREATE TABLE POLITIC ("
                        + "NIF CHAR(9),"
                        + "NOM VARCHAR(50),"
                        + "DATANAIXEMENT DATE,"
                        + "SOU INT,"
                        + "ESCORRUPTE BOOLEAN)";

                Statement statement = con.createStatement();
                //ERROR si la tabla ya existe, da error al crealo
                // pero si no existe, da error al borrarlo
                statement.execute(drop);
                statement.execute(sentenciaSQL);
                System.out.println("Tabla creat correctament");
            } catch (SQLException e) {
                System.out.println("ERROR no s'ha pogut crear la taula correctament");
            }
        } catch (SQLException e) {
            System.out.println("ERROR en la conexió amb la BBDD, comprova que sigui correctes les dades ");
        }
    }

    public static Politic pedirDades() {
        System.out.println("\n==Introdueïx dades del politic==");

        System.out.print("NIF: ");
        String nif = sc.nextLine();

        System.out.print("Nom: ");
        String nom = sc.nextLine();

        System.out.print("Data de naixement (dd/MM/yyyy): ");
        LocalDate data = Leer.pedirData();

        System.out.print("Sou: ");
        int sou = Leer.pedirInt();

        System.out.print("Es corrupte? (s/n): ");
        boolean esCorrupte = Leer.perdirSN();

        return new Politic(nif, nom, data, sou, esCorrupte);
    }


    public static void insertarDadesBBDD(Politic politic) {
        try {
            String sentenciaSQL = "INSERT INTO POLITIC (NIF, NOM, DATANAIXEMENT, SOU, ESCORRUPTE) VALUES (?,?,?,?,?)";

            PreparedStatement preparedStatement = con.prepareStatement(sentenciaSQL);
            preparedStatement.setString(1, politic.getNif());
            preparedStatement.setString(2, politic.getNom());
            preparedStatement.setDate(3, java.sql.Date.valueOf(politic.getDataNaixement()));
            preparedStatement.setInt(4, politic.getSou());
            preparedStatement.setBoolean(5, politic.isEsCorrupte());

            preparedStatement.executeUpdate();
            System.out.println("Dades insertades correctament");
        } catch (SQLException e) {
            System.out.println("ERROR no s'ha pogut insertar la taula correctament");
        }

    }

    public static List<Politic> llegirTaula() {
        List<Politic> politics = new ArrayList<>();
        try {
            String sentenciaSQL = "select * from POLITIC";

            Statement statement = con.createStatement();
            ResultSet rs = statement.executeQuery(sentenciaSQL);
            while (rs.next()) {
                Politic politic = new Politic();
                politic.setNif(rs.getString("NIF"));
                politic.setNom(rs.getString("NOM"));
                politic.setDataNaixement(rs.getDate("DATANAIXEMENT").toLocalDate());
                politic.setSou(rs.getInt("SOU"));
                politic.setEsCorrupte(rs.getBoolean("ESCORRUPTE"));
                politics.add(politic);
            }
        } catch (SQLException e) {
            System.out.println("ERROR no s'ha pogut llegir la taula correctament");
        }

        return politics;
    }
}
