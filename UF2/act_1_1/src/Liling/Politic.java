package Liling;

import java.time.LocalDate;

public class Politic {

    private String nif;
    private String nom;
    private LocalDate dataNaixement;
    private int sou;
    private boolean esCorrupte;

    public Politic(String nif, String nom, LocalDate dataNaixement, int sou, boolean esCorrupte) {
        this.nif = nif;
        this.nom = nom;
        this.dataNaixement = dataNaixement;
        this.sou = sou;
        this.esCorrupte = esCorrupte;
    }

    public Politic() {

    }

    @Override
    public String toString() {
        return
                "     nif='" + nif + '\'' +
                "\n     nom='" + nom + '\'' +
                "\n     dataNaixement=" + dataNaixement +
                "\n     sou=" + sou +
                "\n     esCorrupte=" + esCorrupte+"\n";
    }

    public String getNif() {
        return nif;
    }

    public String getNom() {
        return nom;
    }

    public LocalDate getDataNaixement() {
        return dataNaixement;
    }

    public int getSou() {
        return sou;
    }

    public boolean isEsCorrupte() {
        return esCorrupte;
    }

    public void setNif(String nif) {
        this.nif = nif;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public void setDataNaixement(LocalDate dataNaixement) {
        this.dataNaixement = dataNaixement;
    }

    public void setSou(int sou) {
        this.sou = sou;
    }

    public void setEsCorrupte(boolean esCorrupte) {
        this.esCorrupte = esCorrupte;
    }
}
