package Varies;

import java.text.ParseException;
import java.text.SimpleDateFormat;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.util.Scanner;
import java.util.zip.DataFormatException;

import static java.time.LocalDate.parse;

public class Leer {
    static Scanner sc = new Scanner(System.in);

    public static boolean stringToInt(String cadena) {
        try {
            Integer.parseInt(cadena);
            return true;
        } catch (NumberFormatException e) {
            return false;
        }
    }

    public static int pedirInt() {
        String valor = sc.nextLine();

        int resultat = 0;
        if (stringToInt(valor)) {
            resultat = Integer.parseInt(valor);
        } else {
            System.out.println("ERROR, "+valor+" no es un int.");
            System.out.print("Introdueix un int: ");
            resultat = pedirInt();

        }
        return resultat;
    }


    public static int opcion(int min, int max) {
        int resultat = 0;
        boolean bucle;

        do {
            bucle = true;
            System.out.print("Opcio?: ");
            int num = pedirInt();

            if (num >= min && num <= max) {
                resultat = num;
                bucle = false;
            } else {
                System.out.println("ERROR, introdueix un numero de " + min + "-" + max + ": ");
            }
        }while (bucle);

        return resultat;
    }



    public static LocalDate pedirData() {
        String data = sc.nextLine();
        LocalDate resultat = null;
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy");

        try {
            resultat = LocalDate.parse(data, formatter);
        } catch (DateTimeParseException e) {
            System.out.println("ERROR, introdueix una data correct. ");
            System.out.print("Introdueix la data en format (dd/MM/yyyy): ");
            pedirData();
        }
        return resultat;
    }


    public static boolean perdirSN() {
        String opcion = sc.nextLine();

        boolean resultat = true;
        if (opcion.equals("s") || opcion.equals("S") || opcion.equals("n") || opcion.equals("N")) {
            if (opcion.equals("n") || opcion.equals("N")) {
                resultat = false;
            }
        } else {
            System.out.println("ERROR no has introduít (s/n).");
            System.out.print("Torna a introduïr la opcio amb un 's' o 'n': ");
            perdirSN();
        }
        return resultat;
    }
}


